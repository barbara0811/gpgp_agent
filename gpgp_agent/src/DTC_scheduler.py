#!/usr/bin/env python

"""
Initializes ROS node for generating schedule.

Summary:

    Publications: 
    
    Subscriptions: 
         * /namespace/mission_control [gpgp_agent/MissionCtrlMsg]
         * /namespace/task_structure_update [gpgp_agent/TaskStructureUpdate]
    
    Services: 
         * /namespace/reschedule
         * /namespace/add_non_local_task
         * /namespace/remove_task
         * /namespace/add_commitment
         * /namespace/execute_method
         * /namespace/task_outcome_ev
         
    Parameters:
         param name="pack" -- name of a package which contains mission specific data
         param name="label" -- list of agent labels in taems structure
    
    Arguments:
         arg name="name" 
"""
import sets

__author__ = 'barbanas'

from sys import maxint
import rospy
import rospkg
rospack = rospkg.RosPack()
import taems
import os
import helper_functions
import copy
from datetime import datetime
import genetic_algorithm
import loader
import json

from gpgp_agent.msg import TaskStructureUpdate, MissionCtrlMsg
from gpgp_agent.srv import Reschedule, AddNonLocalTask, RemoveTask, AddCommitmentLocal, TaskOutcomeEV, ExecuteTask

class DTCScheduler(object):
    """
    """

    def __init__(self, maxAlternativeNumber, maxAlternativeNumberRoot):
        
        self.maxAlternativeNumber = maxAlternativeNumber
        self.maxAlternativeNumberRoot = maxAlternativeNumberRoot
        self.criteria = Criteria()
        self.plans = {}
        self.loader = loader.Loader()
        self.treeDict = {} # key: mission id, value: taems tree instance
        self.missionStatus = {}
        self.completedMethods = {}
        
        self.label = rospy.get_param('~label').split(',')
        self.treePathDict = None
        self.dataLocationPackage = rospy.get_param('~pack')
        for item in self.label:
            if self.treePathDict is None:
                self.treePathDict = self.loader.load_filenames(item, rospy.get_namespace(), rospack.get_path(self.dataLocationPackage) + '/data/tree_structures.txt')
            else:
                temp = self.loader.load_filenames(item, rospy.get_namespace(), rospack.get_path(self.dataLocationPackage) + '/data/tree_structures.txt')
                for key in temp.keys():
                    if key in self.treePathDict:
                        self.treePathDict[key].extend(temp[key])
                        self.treePathDict[key] = list(set(self.treePathDict[key]))
                    else:
                        self.treePathDict[key] = temp[key]
        
        rospy.Subscriber("task_structure_update", TaskStructureUpdate, self.update_task_structure)
        rospy.Subscriber("mission_control", MissionCtrlMsg, self.msg_mission_ctrl_callback)
        
        rospy.Service("reschedule", Reschedule, self.create_schedule_srv)
        rospy.Service("add_non_local_task", AddNonLocalTask, self.add_non_local_task_srv)
        rospy.Service("remove_task", RemoveTask, self.remove_task_srv)
        rospy.Service("add_commitment", AddCommitmentLocal, self.add_commitment_srv)
        rospy.Service("task_outcome_ev", TaskOutcomeEV, self.calc_task_outcome_ev_srv)
        rospy.Service("execute_method", ExecuteTask, self.execute_method_srv)
        
        rospy.spin()

    def msg_mission_ctrl_callback(self, msg):
        
        if msg.type == "NewMission":
            self.missionStatus[msg.mission_id] = "wait"
            if msg.mission_id not in self.treeDict.keys():
                self.load_tree(msg.mission_id, msg.root_task)
            self.completedMethods[msg.mission_id] = []
            self.missionStatus[msg.mission_id] = "loaded_tree"
                
        if msg.type == "Abort" or msg.type == "Completed":
            if msg.mission_id in self.treeDict.keys():
                self.missionStatus[msg.mission_id] = "wait"
                self.treeDict.pop(msg.mission_id)
                self.completedMethods.pop(msg.mission_id)
                
        if msg.type == "Restart":
            if msg.mission_id in self.treeDict.keys():
                self.missionStatus[msg.mission_id] = "wait"
                for task in self.treeDict[msg.mission_id].tasks.keys():
                    if self.treeDict[msg.mission_id].tasks[task].subtasks is None:
                        if self.treeDict[msg.mission_id].tasks[task].nonLocal:
                            self.treeDict[msg.mission_id].tasks.pop(task)
                
    def update_task_structure(self, msg):
        
        while msg.mission_id not in self.missionStatus.keys():
            pass 
        
        while self.missionStatus[msg.mission_id] != "loaded_tree":
            pass
        
        self.missionStatus[msg.mission_id] = "wait"
        
        if msg.mission_id not in self.treeDict.keys():
            self.load_tree(msg.mission_id, msg.root_task)
        
        outcomes = json.loads(msg.json_outcome)
        
        for i in range(len(msg.task_label)):
            task = msg.task_label[i]
            outcome = [{},{},{}]
            for item in outcomes[i][0].keys():
                outcome[0][int(float(item))] = outcomes[i][0][item]
            for item in outcomes[i][1].keys():
                outcome[1][int(float(item))] = outcomes[i][1][item]
            for item in outcomes[i][2].keys():
                outcome[2][int(float(item))] = outcomes[i][2][item]
            self.treeDict[msg.mission_id].tasks[task].outcome = outcome
            
            self.treeDict[msg.mission_id].tasks[task].DurationEV = helper_functions.calcExpectedValue(outcome[1])
            self.treeDict[msg.mission_id].tasks[task].CostEV = helper_functions.calcExpectedValue(outcome[2])

        for i in range(len(msg.resource)):
            self.treeDict[msg.mission_id].resources[msg.resource[i]].state = msg.resource_state[i]
            
        self.missionStatus[msg.mission_id] = "ok"
        
    def load_tree(self, missionID, taskLabel):
        ''' Load taems structure. '''
        if missionID not in self.treeDict.keys():
            for filename in self.treePathDict[taskLabel]:
                tree = taems.TaemsTree()
                if filename != "None":
                    self.loader.parse(rospack.get_path(self.dataLocationPackage) + '/data/Missions/', filename, self.label, tree)
                if missionID not in self.treeDict:
                    self.treeDict[missionID] = tree
                else:
                    self.treeDict[missionID] = taems.TaemsTree.merge(self.treeDict[missionID], tree)
                    #print self.treeDict[missionID].tasks.keys()
         
    def add_non_local_task_srv(self, req):
        while req.mission_id not in self.missionStatus.keys():
            pass 
        
        while self.missionStatus[req.mission_id] != "ok":
            pass
        
        task = taems.Method()
        task.label = req.task_label
        task.supertasks = [req.supertask_label]
        q = {req.outcome_ev[0] : 1.0}
        d = {req.outcome_ev[1] : 1.0}
        c = {req.outcome_ev[2] : 1.0} 
        task.outcome = [q, d, c]
        task.nonLocal = True
        
        self.treeDict[req.mission_id].tasks[req.task_label] = task
        
        return True
    
    def remove_task_srv(self, req):
        
        self.treeDict[req.mission_id].removeTaskAndSubtasks(req.task_label)
        return []
       
    def add_commitment_srv(self, req):
        
        while req.mission_id not in self.missionStatus.keys():
            pass 
        
        while self.missionStatus[req.mission_id] != "ok":
            pass
        #print "DTC adding commitment " + req.task_label + "   " + str(req.time)
        self.add_task_commitment(req.mission_id, req.type, req.task_label, req.time, req.break_time)
        return True
    
    def add_task_commitment(self, missionID, commType, task, time, breakTime):
        
        while missionID not in self.missionStatus.keys():
            pass
        
        if commType == "deadline":
            if self.treeDict[missionID].tasks[task].deadline is None or breakTime:
                self.treeDict[missionID].tasks[task].deadline = time
            elif self.treeDict[missionID].tasks[task].deadline > time:
                self.treeDict[missionID].tasks[task].deadline = time
        elif commType == "earliest_start_time":
            if self.treeDict[missionID].tasks[task].earliestStartTime is None or breakTime:
                self.treeDict[missionID].tasks[task].earliestStartTime = time
            elif self.treeDict[missionID].tasks[task].earliestStartTime < time:
                self.treeDict[missionID].tasks[task].earliestStartTime = time
            
        if self.treeDict[missionID].tasks[task].subtasks is None:
            return
        
        for subtask in self.treeDict[missionID].tasks[task].subtasks:
            if subtask not in self.treeDict[missionID].tasks.keys():
                continue
            if self.treeDict[missionID].tasks[subtask].subtasks is None:
                if self.treeDict[missionID].tasks[subtask].nonLocal == True:
                    continue
            self.add_task_commitment(missionID, commType, subtask, time, breakTime)
      
    def execute_method_srv(self, req):
        
        if req.task in self.completedMethods[req.mission_id]:
            return True
        
        self.completedMethods[req.mission_id].append(req.task)
        parent = self.treeDict[req.mission_id].tasks[req.task].supertasks[0]
        self.treeDict[req.mission_id].tasks[parent].subtasks.remove(req.task)
        self.treeDict[req.mission_id].tasks.pop(req.task)
        
        return True
        
    def get_disjunct_task_sets(self, tree):
        sets = self._get_disjunct_task_sets(tree, tree.rootTask[0])
        return sets
        
    def _get_disjunct_task_sets(self, tree, taskLabel):
        
        if type(tree.tasks[taskLabel]) is taems.Method:
            return [[taskLabel]]
        else:
            s = []
            for subtask in tree.tasks[taskLabel].subtasks:
                if subtask in tree.tasks.keys():
                    s.extend(self._get_disjunct_task_sets(tree, subtask))
                else:
                    s.extend([[subtask]])
            if 'seq' not in tree.tasks[taskLabel].qaf:
                ret = [[taskLabel] for i in range(len(s))]
                
                for i in range(len(s)):
                    ret[i].extend(s[i]) 
                return ret
            else:
                ret = [taskLabel]
                for x in s:
                    ret.extend(x)
                return [ret]
        
    def create_schedule_srv(self, req):
        '''
            Args:
                req
                {
                    string mission_id
                    string root_task
                    string criteria
                } 
        '''
        while req.mission_id not in self.missionStatus.keys():
            pass 
        
        while self.missionStatus[req.mission_id] != "ok":
            pass
        
        tree = self.treeDict[req.mission_id]
        
        self.criteria.load(rospack.get_path(self.dataLocationPackage) + '/data/Criteria/' + req.criteria)
        
        ''' step 1: create alternatives '''
        self.plans = {}
        self.missingTasks = []
        disjunctTaskSets = self.get_disjunct_task_sets(tree)
        rootPlan = self.create_alternatives(tree, tree.tasks[tree.rootTask[0]], False, disjunctTaskSets)

        if len(rootPlan.alternatives) == 0:
            print " [DTC] Can't create schedule, agents performing tasks " + str(self.missingTasks) + " are missing!"
            return [-2, [], [], [], [], []]
        
        sumQEV = 0
        for item in rootPlan.qualityEV:
            sumQEV += item
        
        print "PLANS:"
        print rootPlan.alternatives
        print "q"
        print rootPlan.qualityEV
        print "d"
        print rootPlan.durationEV
        print "c"
        print rootPlan.costEV
        
            
        ''' step 2: pick best alternatives to schedule '''
        rating = self.criteria.evaluate_plan(rootPlan)
        print rating
        '''print ""
        print "## ALTERNATIVES"
        for i in range(len(rating)):
            print str(rootPlan.alternatives[i]) + "        " + str(rating[i])
        print "........"
        raw_input("prompt1")'''
        sortedPlan = DTCScheduler.sort_by_rating(rootPlan, rating)

        genAlg = genetic_algorithm.GeneticAlgorithm(30, 0.3, 0.4)
        # TODO non local task is not in tree!? <----
        #print max(rating)
        while len(sortedPlan) > 0:  
            nonLocal = []
            toSchedule = []
            for method in sortedPlan[0].alternatives[0]:
                if self.treeDict[req.mission_id].tasks[method].nonLocal:
                    nonLocal.append(method)
                    continue
                toSchedule.append(method)

            if len(toSchedule) == 0:
                if len(nonLocal) > 0:
                    return [-1, [], [], [], [], []]
                else:
                    return [-2, [], [], [], [], []]

            sched = genAlg.optimize(toSchedule, nonLocal, tree, self.criteria, req.mission_start_time)
            if sched is None:
                # schedule next best alternative
                del sortedPlan[0]
            else:
                break
        #print sched
        #print "//////////////"
        if sched is None:
            print " [DTC] Quality of root task is 0! Schedule won't be created.."
            print " Resources: "
            for resource in tree.resources.values():
                print "    label " +resource.label
                print "    state after scheduling " + str(resource.state)
                print "    allowed range <" + str(resource.depleted_at) + ", " + str(resource.overloaded_at) + ">"
                print "---"
            return [-3, [], [], [], [], []]
        
        taskLabels = []
        startTimes = []
        endTimes = []
        
        for x in sched:
            taskLabels.append(x[0])
            startTimes.append(x[1])
            endTimes.append(x[2])
        
        return [0, taskLabels, startTimes, endTimes, sortedPlan[0].completedTasks[0], nonLocal]

    def calc_task_outcome_ev_srv(self, req):
        '''
        Calculates task outcome as average outcome of all alternatives (plans) that complete task. 
        '''
        while req.mission_id not in self.missionStatus.keys():
            pass 
        
        while self.missionStatus[req.mission_id] != "ok":
            pass
        
        tree = self.treeDict[req.mission_id]
        self.criteria.load(rospack.get_path(self.dataLocationPackage) + '/data/Criteria/' + req.criteria)
        
        ''' step 1: create alternatives '''
        self.plans = {}
        self.missingTasks = []
        disjunctTaskSets = self.get_disjunct_task_sets(tree)
        self.create_alternatives(tree, tree.tasks[tree.rootTask[0]], True, disjunctTaskSets)
        tasks = self.plans.keys()
        averageEV = []
        for task in tasks:
            q = self.plans[task].qualityEV
            d = self.plans[task].durationEV
            c = self.plans[task].costEV
            
            if len(q) == 0:
                averageEV.append([0, 0, 0])
                print " [DTC scheduler - ERROR] taems is not defined correctly! "
                print task
                print q
                print self.plans[task].alternatives
                print self.treeDict[req.mission_id].tasks[task].subtasks
                print self.treeDict[req.mission_id].tasks[task].supertasks
                print self.treeDict[req.mission_id].tasks[task].qaf
                return [[], json.dumps([])]
            
            toRemove = []
            for i in range(len(q)):
                if q[i] == 0 or d[i] > pow(10, 5) or c[i] > pow(10, 5):
                    toRemove.append(i) 
            removed = 0
            for i in toRemove:
                del q[i - removed]
                del d[i - removed]
                del c[i - removed]
                removed += 1
                
            self.plans[task].qualityEV = q
            self.plans[task].durationEV = d
            self.plans[task].costEV = c

            if len(q) == 0:
                averageEV.append([0, maxint, maxint])
            else:
                averageEV.append([sum(q)/len(q), sum(d)/len(d), sum(c)/len(c)])

        return [tasks, json.dumps(averageEV)]
        
    @staticmethod
    def sort_by_rating(plan, rating):
        '''
        plan : Plan to sort
        rating: list of alternative ratings
        '''
        temp = copy.deepcopy(rating)
        sortedPlan = []

        for i in range(len(rating)):
            index = temp.index(max(temp))

            p = Plan(plan.task)
            p.alternatives = [plan.alternatives[index]]
            p.cost = [plan.cost[index]]
            p.duration = [plan.duration[index]]
            p.quality = [plan.quality[index]]
            p.costEV = [plan.costEV[index]]
            p.durationEV = [plan.durationEV[index]]
            p.qualityEV = [plan.qualityEV[index]]
            p.subtasks = plan.subtasks
            p.methods = plan.methods
            p.completedTasks = [plan.completedTasks[index]]
            p.childrenAlternativeIndex = [plan.childrenAlternativeIndex[index]]

            sortedPlan.append(p)
            temp[index] = -1

        return sortedPlan

    def create_alternatives(self, tree, root, ignoreNonLocal, disjunctTaskSets):
        '''
        Method for creating alternatives "bottom - up" -> recursively.
        
        Args:
            tree - TaemsTree    
            root - TaskGroup / Method
        '''
        #stopping condition
        if type(root) is taems.Method:
            myPlan = Plan(root.label)
            myPlan.quality = [tree.tasks[root.label].outcome[0]]
            myPlan.duration = [tree.tasks[root.label].outcome[1]]
            myPlan.cost = [tree.tasks[root.label].outcome[2]]
            myPlan.alternatives = [[root.label]]
            myPlan.methods.append(root.label)
            myPlan.completedTasks.append([])
            myPlan.qualityEV = [helper_functions.calcExpectedValue(myPlan.quality[0])]
            myPlan.costEV = [helper_functions.calcExpectedValue(myPlan.cost[0])]
            myPlan.durationEV = [helper_functions.calcExpectedValue(myPlan.duration[0])]

            self.plans[root.label] = myPlan
            return

        #create a plan instance
        myPlan = Plan(root.label)

        index = 0
        #go through every child:
        for i in range(len(root.subtasks)):
            if root.subtasks[i] in tree.tasks.keys():
                myPlan.subtasks.append(root.subtasks[i])
                self.create_alternatives(tree, tree.tasks[root.subtasks[i]], ignoreNonLocal, disjunctTaskSets)
                myPlan.methods.extend(self.plans[root.subtasks[i]].methods)
                #childrenPlansMethods.append(childrenPlans[index].alternatives)
                index += 1
            else:
                if ignoreNonLocal == False:
                    if root.qaf == "q_min" or "_all" in root.qaf:
                        self.missingTasks.append(root.label)
                        myPlan = Plan(root.label)
                        self.plans[root.label] = myPlan
                        return myPlan

        for i in range(len(myPlan.subtasks)):
            plan = self.plans[myPlan.subtasks[i]]
            myPlan.childrenAlternativeIndex.append([])
            for j in range(len(plan.alternatives)):
                myPlan.childrenAlternativeIndex[-1].append([i,j])

        if root.qaf == 'q_sum_all' or root.qaf == 'q_min' or root.qaf == 'q_seq_sum_all':
            #all children's plans MUST be included in parent's plan
            myPlan.childrenAlternativeIndex = helper_functions.cartesianProduct(myPlan.childrenAlternativeIndex, False, False)
        
        elif root.qaf == 'q_max':
            #only one children's plan MUST be included
            temp = []
            for item in myPlan.childrenAlternativeIndex:
                if type(item) is list:
                    for element in item:
                        temp.append([element])
                else:
                    temp.append([item])

            myPlan.childrenAlternativeIndex = temp
            #myPlan.childrenAlternativeIndex = helper_functions.OR(myPlan.childrenAlternativeIndex)
            pass    

        elif root.qaf == 'q_sum':
            #power set of children's plans
            temp = []
            for x in helper_functions.powerSet(myPlan.childrenAlternativeIndex):
                temp.append(x)
                
            #get rid of excess of lists crated by function powerSet
            for i in range(len(temp)):
                for j in range(len(temp[i])):
                    temp[i][j] = temp[i][j][0]

            myPlan.childrenAlternativeIndex = temp

        if len(myPlan.childrenAlternativeIndex) > 0:
            myPlan.evaluate(root.qaf, self.plans, disjunctTaskSets)

            if len(root.supertasks) > 0:
                self.criteria.trim_alternatives(myPlan, self.maxAlternativeNumber)
            else:
                self.criteria.trim_alternatives(myPlan, self.maxAlternativeNumberRoot)
            
            #add actual methods into alternatives
            for alternative in myPlan.childrenAlternativeIndex:
                myPlan.alternatives.append([])
                myPlan.completedTasks.append([])
                myPlan.completedTasks[-1].append(root.label) # myPlan.subtasks[method[0]])
                for method in alternative:
                    alt = self.plans[myPlan.subtasks[method[0]]].alternatives[method[1]]
                    myPlan.alternatives[-1].extend(alt)
                    myPlan.completedTasks[-1].extend(self.plans[myPlan.subtasks[method[0]]].completedTasks[method[1]])

            if len(myPlan.alternatives) > 0:
                toRemove = []
                for i in range(len(myPlan.alternatives)):
                    if i in toRemove:
                        continue
                    for item in tree.mutuallyExclusiveTasks:
                        tasks = set(myPlan.completedTasks[i]) | set(myPlan.alternatives[i])
                        if len(set(item) & tasks) > 1:
                            toRemove.append(i)
                            break

                removed = 0
                for i in toRemove:
                    myPlan.remove_alternative(i - removed)
                    removed += 1
                    
        self.plans[root.label] = myPlan
        return myPlan

    @staticmethod
    def print_to_file(listOfPlans):
        fileName = os.getcwd() + "\Output\Output_plans " + str(datetime.now())[:-7].replace(':','-')
        file1 = open(fileName, 'w')
        file2 = open(os.getcwd() + "\Outcome_lastOutput.txt", 'w');

        file1.write(str(listOfPlans[0].qualityEV) + "\t")
        file1.write(str(listOfPlans[0].durationEV) + "\t")
        file1.write(str(listOfPlans[0].costEV) + "\n")
            
        for plan in listOfPlans:
            file1.write(str(plan.qualityEV) + "\t")
            file1.write(str(plan.durationEV) + "\t")
            file1.write(str(plan.costEV) + "\n")
            
            file2.write(str(plan.qualityEV) + "\t")
            file2.write(str(plan.durationEV) + "\t")
            file2.write(str(plan.costEV) + "\n")

        file1.close()
        file2.close()        

class Criteria(object):
    '''Criteria models criteria sliders to calculate goodness of each alternative.
        For more info, please consult manual on DTC criteria sliders.
    '''

    def __init__(self):
        self.rawGoodnesQ = 0
        self.rawGoodnesC = 0
        self.rawGoodnesD = 0
        self.thresholdQ = [0,0]
        self.limitC = [0,0]
        self.limitD = [0,0]
        self.certaintyQ = 0
        self.certaintyC = 0
        self.certaintyD = 0
        self.certaintyThresholdQ = [0,0]
        self.certaintyThresholdC = [0,0]
        self.certaintyThresholdD = [0,0]
        self.metaRawGoodnes = 0
        self.metaLimitThreshold = 0
        self.metaCertainty = 0
        self.metaCertaintyThresholds = 0

    def load(self, filename):
        '''Loads criteria from file given with filename. A structure of criteria
            specification file and an example can be found in documentation.
            
            Args:
                filename: A string
                
            Returns:
                -
        '''
        f = open(filename, 'r')
        lines = f.readlines()

        index = Criteria.skip_lines( lines, 0)
        self.rawGoodnesQ = float(lines[index].strip())

        index = Criteria.skip_lines( lines, index + 1)
        self.rawGoodnesC = float(lines[index].strip())
        
        index = Criteria.skip_lines( lines, index + 1)
        self.rawGoodnesD = float(lines[index].strip())

        index = Criteria.skip_lines( lines, index + 1)

        if lines[index].strip()[0] != '0':
            array = lines[index].strip().split(' ')
            self.thresholdQ[0] = float(array[0])
            self.thresholdQ[1] = float(array[1])

        index = Criteria.skip_lines( lines, index + 1)
        if lines[index].strip()[0] != '0':
            array = lines[index].strip().split(' ')
            self.limitC[0] = float(array[0])
            self.limitC[1] = float(array[1])

        index = Criteria.skip_lines( lines, index + 1)
        if lines[index].strip()[0] != '0':
            array = lines[index].strip().split(' ')
            self.limitD[0] = float(array[0])
            self.limitD[1] = float(array[1])

        index = Criteria.skip_lines( lines, index + 1)
        self.certaintyQ = float(lines[index].strip())

        index = Criteria.skip_lines( lines, index + 1)
        self.certaintyC = float(lines[index].strip())

        index = Criteria.skip_lines( lines, index + 1)
        self.certaintyD = float(lines[index].strip())

        index = Criteria.skip_lines( lines, index + 1)
        if lines[index].strip()[0] != '0':
            array = lines[index].strip().split(' ')
            self.certaintyThresholdQ[0] = float(array[0])
            self.certaintyThresholdQ[1] = float(array[1])

        index = Criteria.skip_lines( lines, index + 1)
        if lines[index].strip()[0] != '0':
            array = lines[index].strip().split(' ')
            self.certaintyThresholdC[0] = float(array[0])
            self.certaintyThresholdC[1] = float(array[1])

        index = Criteria.skip_lines( lines, index + 1)
        if lines[index].strip()[0] != '0':
            array = lines[index].strip().split(' ')
            self.certaintyThresholdD[0] = float(array[0])
            self.certaintyThresholdD[1] = float(array[1])

        index = Criteria.skip_lines( lines, index + 1)
        self.metaRawGoodnes = float(lines[index].strip())

        index = Criteria.skip_lines( lines, index + 1)
        self.metaLimitThreshold = float(lines[index].strip())

        index = Criteria.skip_lines( lines, index + 1)
        self.metaCertainty = float(lines[index].strip())

        index = Criteria.skip_lines( lines, index + 1)
        self.metaCertaintyThresholds = float(lines[index].strip())

    @staticmethod
    def skip_lines(lines, index):

        while index < len(lines):
            if len(lines[index].strip()) == 0:
                index += 1
            elif lines[index][0] == "#":
                index += 1
            else:
                return index
        return index

    def evaluate_plan(self, plan):
        '''Evaluates a plan by criteria values.
        
            Args:
                plan - A plan object to evaluate
            
            Returns:
                totalRating - A list of float values, ratings for every alternative in the given plan
        '''
        return self.evaluate(plan.quality, plan.duration, plan.cost, plan.qualityEV, plan.durationEV, plan.costEV)

    def evaluate(self, quality, duration, cost, qualityEV, durationEV, costEV):
        '''
        '''
        minQ = min(qualityEV)
        minC = min(costEV)
        minD = min(durationEV)

        maxQ = max(qualityEV)
        maxC = max(costEV)
        maxD = max(durationEV)

        # calculate raw goodness:
        sumRG = self.rawGoodnesC + self.rawGoodnesD + self.rawGoodnesQ

        ratingRG = []
        if sumRG > 0:
            ratingRGQ = []
            ratingRGC = []
            ratingRGD = []

            test1 = False
            test2 = False
            test3 = False
            if maxQ == minQ:
                test1 = True
            if maxC == minC:
                test2 = True
            if maxD == minD:
                test3 = True

            for i in range(len(qualityEV)):
                if test1:
                    ratingRGQ.append(1)
                else:
                    ratingRGQ.append(((qualityEV[i] - minQ)/(maxQ - minQ))*(self.rawGoodnesQ / sumRG))
                if test2:
                    ratingRGC.append(1)
                else:
                    ratingRGC.append(((maxC - costEV[i])/(maxC - minC))*(self.rawGoodnesC / sumRG))
                if test3:
                    ratingRGD.append(1)
                else:
                    ratingRGD.append(((maxD - durationEV[i])/(maxD - minD))*(self.rawGoodnesD / sumRG))

                ratingRG.append(ratingRGQ[i] + ratingRGC[i] + ratingRGD[i])

        #calculate thresholds and limits
        sumTL = self.thresholdQ[0] + self.limitC[0] + self.limitD[0]

        ratingTL = []
        if sumTL > 0:
            
            ratingTLQ = []
            ratingTLC = []
            ratingTLD = []

            for i in range(len(qualityEV)):
                if qualityEV[i] > self.thresholdQ[1] :
                    ratingTLQ.append(self.thresholdQ[0] / sumTL)
                else:
                    ratingTLQ.append(0)

                if costEV[i] < self.limitC[1] :
                    ratingTLC.append(self.limitC[0] / sumTL)
                else:
                    ratingTLC.append(0)

                if durationEV[i] < self.limitD[1] :
                    ratingTLD.append(self.limitD[0] / sumTL)
                else:
                    ratingTLD.append(0)
           
                ratingTL.append(ratingTLQ[i] + ratingTLC[i] + ratingTLD[i])

        #calculate certainty
        sumCE = self.certaintyC + self.certaintyD + self.certaintyQ

        ratingCE = []
        if sumCE > 0:
            probQ = []  
            probC = []
            probD = []
            for i in range(len(qualityEV)):
                probQ.append(helper_functions.probXGreaterThanVal(quality[i], qualityEV[i]))
                probC.append(helper_functions.probXSmallerThanVal(cost[i], costEV[i]))
                probD.append(helper_functions.probXSmallerThanVal(duration[i], durationEV[i]))

            minProbQ = min(probQ)
            minProbC = min(probC)
            minProbD = min(probD)

            maxProbQ = max(probQ)
            maxProbC = max(probC)
            maxProbD = max(probD)

            ratingCEQ = []
            ratingCEC = []
            ratingCED = []

            test1 = False
            test2 = False
            test3 = False
            if maxProbQ == minProbQ:
                test1 = True
            if maxProbC == minProbC:
                test2 = True
            if maxProbD == minProbD:
                test3 = True

            for i in range(len(qualityEV)):
                if test1:
                    ratingCEQ.append(1)
                else:
                    ratingCEQ.append(((probQ[i] - minProbQ) / (maxProbQ - minProbQ))* (self.certaintyQ / sumCE))
                if test2:
                    ratingCEC.append(1)
                else:
                    ratingCEC.append(((probC[i] - minProbC) / (maxProbC - minProbC))* (self.certaintyC / sumCE))
                if test3:
                    ratingCED.append(1)
                else:
                    ratingCED.append(((probD[i] - minProbD) / (maxProbD - minProbD))* (self.certaintyD / sumCE))

                ratingCE.append(ratingCEQ[i] + ratingCEC[i] + ratingCED[i])

        #calculate certainty threshold
        sumCET = self.certaintyThresholdC[0] + self.certaintyThresholdD[0] + self.certaintyThresholdQ[0]

        ratingCET = []
        if sumCET > 0:
            
            ratingCETQ = []
            ratingCETC = []
            ratingCETD = []

            for i in range(len(qualityEV)):
                if probQ[i] > self.certaintyThresholdQ[1] :
                    ratingCETQ.append(self.certaintyThresholdQ[0] / sumTL)
                else:
                    ratingCETQ.append(0)

                if probC[i] < self.certaintyThresholdC[1] :
                    ratingCETC.append(self.certaintyThresholdC[0] / sumTL)
                else:
                    ratingCETC.append(0)

                if probD[i] < self.certaintyThresholdD[1] :
                    ratingCETD.append(self.certaintyThresholdD[0] / sumTL)
                else:   
                    ratingCETD.append(0)

                ratingCET.append(ratingCETQ[i] + ratingCETC[i] + ratingCETD[i])

        totalRating = []
        for i in range(len(qualityEV)):
            if len(ratingRG) > 0: 
                first = ratingRG[i]
            else: 
                first = 0

            if len(ratingTL) > 0: 
                second = ratingTL[i]
            else: 
                second = 0

            if len(ratingCE) > 0: 
                third = ratingCE[i]
            else: 
                third = 0

            if len(ratingCET) > 0: 
                fourth = ratingCET[i]
            else: 
                fourth = 0
                
            totalRating.append( first * self.metaRawGoodnes + second * self.metaLimitThreshold + third * self.metaCertainty + fourth * self.metaCertaintyThresholds)

        return totalRating

    def trim_alternatives(self, plan, n):
        '''Trims worst alternatives (the ones with lowest rating) so that plan consists of
            maximum n alternatives.
            
            Args:
                plan - A plan object
                n - An integer
        '''
        if n == -1:
            return

        ratings = self.evaluate_plan(plan)

        while len(plan.alternatives) > n:
            i = ratings.index(min(ratings))
            plan.remove_alternative(i)
            del ratings[i]

class Plan(object):
    '''Plan is a class that serves as a container for alternatives (unordered lists of methods) that
        lead to completion of the task.
        
        Attributes:
            alternatives - a list of alternatives -> [['method1','m2', ...], ['m3','m4', ...], ['m2','m3','m4', ...], ...]
            quality - a list of dictionaries, quality probability distributions
            cost - a list of dictionaries, cost probability distributions
            duration - a list of dictionaries, duration probability distributions
            qualityEV - A list of float numbers, expected values of quality for every alternative
            costEV - A list of float numbers, expected values of cost for every alternative
            durationEV - A list of float numbers, expected values of duration for every alternative
            treeDict - TaemsTree object
    '''
    def __init__(self, task):
        # list of alternatives --> alternative is a list of unordered methods
        self.task = task
        self.childrenAlternativeIndex = []
        self.alternatives = []  # [['method1','m2', ...], ['m3','m4', ...], ['m2','m3','m4', ...], ...]
        self.quality = []
        self.cost = []
        self.duration = []
        self.qualityEV = []
        self.costEV = []
        self.durationEV = []
        self.subtasks = []
        self.methods = []
        self.completedTasks = []

    def evaluate(self, qaf, plans, disjunctTaskSets):
        '''Calculates quality, cost, duration (probability distribution) for every 
                alternative (list of methods) in self.alternatives -> by calling calc_QCD method.
            Calculates quality, cost and duration expected values for every alternative.
            
            Args:
                qaf - string, predefined set of values for quality accumulation function of a task
                plans - dictionary of plans for every task
        '''
        self.quality = []
        self.cost = []
        self.duration = []

        self.qualityEV = []
        self.costEV = []
        self.durationEV = []
        
        i = 0
        for alternative in self.childrenAlternativeIndex:
            temp = self.calc_QCD(qaf, alternative, plans, disjunctTaskSets)

            self.quality.append(temp[0])
            self.duration.append(temp[1])
            self.cost.append(temp[2])
            
            self.qualityEV.append(helper_functions.calcExpectedValue(self.quality[i]))
            self.costEV.append(helper_functions.calcExpectedValue(self.cost[i]))
            self.durationEV.append(helper_functions.calcExpectedValue(self.duration[i]))

            i += 1
            
    def calc_QCD(self, qaf, alternative, plans, disjunctTaskSets):
        ''' Calculates quality , cost, duration (probability distribution) for 
            alternative (list of methods) with respect of given qaf.
            
            Args:
                qaf - string, predefined set of values for quality accumulation function of a task
                alternative - a list of method labels
                plans - dictionary of plans for every task
        '''
        qualityDistributions = []
        costDistributions = []
        durationDistributions = []
        
        childWithZeroQ = False
        d = [[] for i in range(len(disjunctTaskSets))]
        durDistr = [[] for i in range(len(disjunctTaskSets))]
        for i in alternative:
            child = i[0]
            ind = -1
            for j in range(len(disjunctTaskSets)):
                if self.subtasks[child] in disjunctTaskSets[j]:
                    d[j].append(child)
                    ind = j
                    
            childsAlternative = i[1]
            qualityDistributions.append(plans[self.subtasks[child]].quality[childsAlternative])
            costDistributions.append(plans[self.subtasks[child]].cost[childsAlternative])
            durationDistributions.append(plans[self.subtasks[child]].duration[childsAlternative])
            durDistr[ind].append(plans[self.subtasks[child]].duration[childsAlternative])
            if plans[self.subtasks[child]].qualityEV == 0:
                childWithZeroQ = True
                     
        fun = ""
        if qaf == "q_min":
            fun = "min"
        elif qaf == "q_max":
            fun = "max"
        elif qaf == 'q_sum':
            fun = "sum"
        elif qaf == 'q_sum_all' or qaf == "q_seq_sum_all":
            fun = "sum"
            if childWithZeroQ:
                return [{0 : 1.0}, {maxint : 1.0}, {maxint : 1.0}]
        
        
        Q = helper_functions.cartesianProductOfDistributions(qualityDistributions, fun)
        C = helper_functions.cartesianProductOfDistributions(costDistributions, "sum")
        Ds = []
        for i in range(len(durDistr)):
            if len(durDistr[i]) > 1:
                Ds.append(helper_functions.cartesianProductOfDistributions(durDistr[i], "sum"))
            elif len(durDistr[i]) == 1:
                Ds.append(durDistr[i][0])
        
        if len(Ds) > 1:
            D = helper_functions.cartesianProductOfDistributions(Ds, "max")
        else:
            D = Ds[0]
        #D = helper_functions.cartesianProductOfDistributions(durationDistributions, "sum") #TODO !!! duration is NOT a sum !!!
        
        return [Q, D, C]
    
    def remove_alternative(self, i):
        '''Removes an alternative at index i from a plan.
        '''
        del self.quality[i]
        del self.cost[i]
        del self.duration[i]
        del self.qualityEV[i]
        del self.costEV[i]
        del self.durationEV[i]
        del self.completedTasks[i]
            
        del self.alternatives[i]

if __name__ == "__main__":
    
    rospy.init_node("scheduler")
    
    try:
        dtc_scheduler = DTCScheduler(-1, -1)
    except rospy.ROSInterruptException:
        pass
