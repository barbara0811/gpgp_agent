#!/usr/bin/env python

"""
Initializes ROS node for GPGP coordination.

Summary:

    Publications: 
         * /peer_discovery [gpgp_agent/HelloMsg]
         * /namespace/mission_enabled_tasks [gpgp_agent/MissionEnabledTasks]
         * /mission_control [gpgp_agent/MissionCtrlMsg]
         * /namespace/mission_control [gpgp_agent/MissionCtrlMsg]

    Subscriptions: 
         * /gazebo/model_states [unknown type] ###TODO -- unified topic named /namespace/position 
                                                that is the same for simulation and real system
         * /namespace/task_structure_update [gpgp_agent/TaskStructureUpdate]
         * /peer_discovery [gpgp_agent/HelloMsg]
         * /mission_control [gpgp_agent/MissionCtrlMsg]

    Services: 
         * /namespace/task_info
         * /namespace/signal_abort
         * /namespace/schedule_ok
         * /namespace/mission_status
         * /namespace/register_executed_task
         * /namespace/resolve_redundant
         * /namespace/mission_info
         * /namespace/register_commitment
         * /namespace/register_feedback_request
         * /namespace/get_position

   Parameters:
         param name="pack" -- name of a package which contains mission specific data
         param name="label" -- list of agent labels in taems structure
    
    Arguments:
         arg name="name"
"""

__author__ = 'barbanas'

import rospy
import rospkg
rospack = rospkg.RosPack()

import sys
sys.path.append(rospack.get_path("gpgp_agent") + "/src")
import loader
import taems
import json
import simulator
import helper_functions
from DTC_scheduler import Criteria
import time
from copy import deepcopy
import numpy as np
from branch_and_bound import BranchAndBoundOptimizer
from sys import maxint

from gpgp_agent.msg import TaskStructureUpdate, MissionCtrlMsg, HelloMsg, MissionEnabledTasks
from geometry_msgs.msg import TwistStamped, Point
from gpgp_agent.srv import Reschedule, AssessMissionTasks, ReassessMissionTasks, TaskInfo, RedundancySolution, AddNonLocalTask, RemoveTask, \
                TaskOutcomeEV, SignalMissionAbort, GetPosition, GetPose, DelaySchedule, MissionInfo, MissionStatus, AddCommitmentLocal, \
                AddCommitmentNonLocal, ScheduleOK, AdjustScheduleTimes, MissionEarliestStart, ExecuteTask, RegisterFeedbackRequest
from gazebo_msgs.msg import ModelStates

class GPGPCoordinator():

    def __init__(self):

        self.serviceTimeout = 0.2
        
        self.label = rospy.get_param('~label').split(',') # label is taems structure agent label => agent type
        self.ns = rospy.get_namespace()
        self.missionCaller = {}
        self.missionType = {}
        self.poseEstimate = None
        
        self.startTime = {}
        
        # task structure
        self.loader = loader.Loader()
        self.treePathDict = None
        self.dataLocationPackage = rospy.get_param('~pack')
        for item in self.label:
            if self.treePathDict is None:
                self.treePathDict = self.loader.load_filenames(item, self.ns, rospack.get_path(self.dataLocationPackage) + '/data/tree_structures.txt')
            else:
                temp = self.loader.load_filenames(item, self.ns, rospack.get_path(self.dataLocationPackage) + '/data/tree_structures.txt')
                for key in temp.keys():
                    if key in self.treePathDict:
                        self.treePathDict[key].extend(temp[key])
                        self.treePathDict[key] = list(set(self.treePathDict[key]))
                    else:
                        self.treePathDict[key] = temp[key]
        self.treeDict = {} # key : mission mission ID, value : TaemsTree instance
        self.treeDictScheduled = {} # key : mission mission ID, value : TaemsTree instance, IR effects of schedule are applied to this structure
        self.taskOutcome = {} # outcome for scheduled tasks, key: mission ID, value: dictionary {key: task label, value: [quality_distribution, duration_distribution, cost_distribution} 
        self.taskOutcomeEVNonSched = {} # expected value for q, d, c of non-scheduled tasks, key: mission ID, value: dictionary {key: task label, value: [qEV, dEV, cEV]}
        self.missions = {} # key: mission ID, value : task label
        self.criteria = {} # key: mission ID, value : Criteria object
        self.criteriaFilename = {} # key: mission mission ID, value : criteria filename
        self.waitTaskAssessmentFlag = {}
        self.waitMissionHandleRedundancy = {}
        # schedules
        self.bestSchedule = {} # key : mission ID, value : schedule for this mission - schedule: [[method1, start_time1, end_time1], [method2, start_time2, end_time2], ... ]
        self.completedTasks = {} # key : mission ID, value : list of completed tasks in a mission
        self.scheduledMethods = {} # key : mission ID, value : list of methods in schedule
        self.waitRedundant = {} # key: mission ID, value: number of redundant tasks to be resolved
        self.taskEndTime = {} # # key: mission ID, value: dictionary {key: task label, value : task end time in mission schedule}
        self.completedNonLocal = {}

        # coordination
        self.neighbors = {}  # key : mission ID, value: dictionary {key: neighbor address (namespace), value: Agent class instance}
        self.parentChildCRs = {} # key: mission ID, value: dictionary {key: non local task, value: [neighbor label, neighbor address]}
        self.commitmentsLocal = {} # key: mission ID, value: LocalCommitment object
        self.commitmentsNonLocal = {} # key: mission ID, value: NonLocalCommitment object
        self.redundantTasks = {} # key: mission ID, value: dictionary {key: task label, value: neighbor address}
        self.responsibleFor = {} # key: mission ID, value: task label -> redundant task whose resolution this agent is responsible for 
        self.missionStatus = {} # key: mission ID, value: mission status, string
        self.scheduleIsCoordinated = {} # key: mission ID, value: boolean, true if the schedule is coordinated
        self.hardConstrainedNeighbors = {} # key: mission ID, value: a list of neighbors with hard constrained coordination relationships
        self.waitForScheduleOK = {} # key: mission ID, value: a list of neighbors whose schedule_ok signal agent is waiting for
        self.waitComplexRedundancy = {}
        self.feedbackRequestList = {}

        # execution
        self.simulator = {}
      
        # task structure
        rospy.Subscriber("task_structure_update", TaskStructureUpdate, self.update_task_structure_callback)     
        # mission control
        self.missionCtrlMsgPub = rospy.Publisher("/mission_control", MissionCtrlMsg, queue_size = 1)
        rospy.Subscriber("/mission_control", MissionCtrlMsg, self.msg_mission_ctrl_callback)
        self.missionCtrlMsgLocalPub = rospy.Publisher("mission_control", MissionCtrlMsg, queue_size = 2)
        
        # coordination
        self.broadcastHelloPub = rospy.Publisher("/peer_discovery", HelloMsg, queue_size = 5)
        rospy.Subscriber("/peer_discovery", HelloMsg, self.msg_hello_callback)
        rospy.Service("task_info", TaskInfo, self.task_info_srv)
        rospy.Service("mission_info", MissionInfo, self.mission_info_srv)
        rospy.Service("mission_status", MissionStatus, self.mission_status_srv)
        rospy.Service("register_feedback_request", RegisterFeedbackRequest, self.register_feedback_request_srv)
        
        rospy.Service("resolve_redundant", RedundancySolution, self.resolve_redundant_srv)
        rospy.Service("register_commitment", AddCommitmentNonLocal, self.add_commitment_non_local_srv)
        rospy.Service("schedule_ok", ScheduleOK, self.register_schedule_ok_srv)
        
        rospy.Service("signal_abort", SignalMissionAbort, self.register_abort_srv)
        rospy.Service("get_position", GetPosition, self.get_position_srv)
        rospy.Service("get_pose", GetPose, self.get_pose_srv)
        
        # execution
        self.missionEnabledTasksPub = rospy.Publisher("mission_enabled_tasks", MissionEnabledTasks, queue_size = 1)
        rospy.Service("register_executed_task", ExecuteTask, self.register_executed_task_srv)
    
    def load_tree(self, missionID, taskLabel):
        ''' Load taems structure for mission. '''
        
        if missionID not in self.treeDict.keys():
            if taskLabel not in self.treePathDict.keys():
                #print self.treePathDict.keys()
                return -1
            for filename in self.treePathDict[taskLabel]:
                tree = taems.TaemsTree()
                if filename != "None":
                    self.loader.parse(rospack.get_path(self.dataLocationPackage) + '/data/Missions/', filename, self.label, tree)
                if missionID not in self.treeDict:
                    self.treeDict[missionID] = tree
                else:
                    self.treeDict[missionID] = taems.TaemsTree.merge(self.treeDict[missionID], tree)
        return 0
            
    def load_criteria(self, missionID, criteriaFiename):
        
        self.criteria[missionID] = Criteria()
        self.criteria[missionID].load(rospack.get_path(self.dataLocationPackage) + '/data/Criteria/' + criteriaFiename)
            
    def init_mission_structures(self, missionID):
        
        self.missionStatus[missionID] = "start"
        self.neighbors[missionID] = {}
        
        self.parentChildCRs[missionID] = {}
        self.commitmentsLocal[missionID] = {}
        self.commitmentsNonLocal[missionID] = {}
        self.redundantTasks[missionID] = {}
        self.responsibleFor[missionID] = set()
        self.feedbackRequestList[missionID] = {}
        
        self.waitRedundant[missionID] = set()
        self.hardConstrainedNeighbors[missionID] = [set(), set()]   # [outgoing, incoming]
        self.waitForScheduleOK[missionID] = set()
        self.waitComplexRedundancy[missionID] = False
        self.scheduleIsCoordinated[missionID] = False
        
        self.simulator[missionID] = None
             
    def abort_mission(self, missionID, restart):
        
        self.missionStatus[missionID] = "no_mission"
        msg = MissionCtrlMsg()
        if restart:
            msg.type = "Restart"
        else:
            msg.type = "Abort"
        msg.ag_addr = self.ns
        msg.mission_id = missionID
        msg.root_task = ""
        msg.criteria = "" 
        
        self.missionCtrlMsgLocalPub.publish(msg)
        
        self.missions.pop(missionID)
         
        self.neighbors.pop(missionID)
        self.parentChildCRs.pop(missionID)
        self.commitmentsLocal.pop(missionID)
        self.commitmentsNonLocal.pop(missionID)
        self.redundantTasks.pop(missionID)
        self.responsibleFor.pop(missionID)
        self.feedbackRequestList.pop(missionID)
        
        if restart == False:
            if missionID in self.treeDict.keys():
                self.treeDict.pop(missionID)
        if missionID in self.treeDictScheduled.keys():
            self.treeDictScheduled.pop(missionID)
        if missionID in self.criteria.keys():
            self.criteria.pop(missionID)
        
        if missionID in self.bestSchedule.keys():
            self.bestSchedule.pop(missionID)
        if missionID in self.completedTasks.keys():
            self.completedTasks.pop(missionID)
        if missionID in self.completedNonLocal:
            self.completedNonLocal.pop(missionID)
        if missionID in self.scheduledMethods.keys():
            self.scheduledMethods.pop(missionID)
        
        if missionID in self.taskOutcomeEVNonSched.keys():
            self.taskOutcomeEVNonSched.pop(missionID)
        if missionID in self.taskEndTime.keys():
            self.taskEndTime.pop(missionID)
        
        self.hardConstrainedNeighbors.pop(missionID)
        self.waitForScheduleOK.pop(missionID)
        self.scheduleIsCoordinated.pop(missionID)
        self.waitComplexRedundancy.pop(missionID)
        
        self.simulator.pop(missionID)
        
        if restart:
            print "\n RESTARTED MISSION " + missionID
        else:
            print "\n CANCELED MISSION " + missionID
        
    def start_new_mission(self, missionID, rootTask, criteria):
        '''
        A method that starts new mission with a root task 'task' and scheduling criteria filename 'criteria'.
        '''
        self.startTime[missionID] = rospy.get_time()
        print " NEW MISSION    {id: " + missionID + ", task: " + rootTask + ", criteria "+ criteria + "}\n"
        self.init_mission_structures(missionID)
        
        # send mission control message to agent's nodes
        msg = MissionCtrlMsg()
        msg.type = "NewMission"
        msg.ag_addr = self.missionCaller[missionID]
        msg.mission_id = missionID
        msg.root_task = rootTask
        msg.criteria = criteria
        
        self.missionCtrlMsgLocalPub.publish(msg)
        
        self.missions[missionID] = rootTask
        self.criteriaFilename[missionID] = criteria
        
        # load taems tree
        if self.load_tree(missionID, rootTask) == -1:
            print " ERROR: the agent does not have the taems tree structure for task " + rootTask
            self.missionStatus[missionID] = "no_schedule"
            self.abort_mission(missionID, False)
            return
          
        #load criteria
        self.load_criteria(missionID, criteria)
        
        print " [GPGP] COORDINATING MISSION    " + missionID + "\n"
    
        # detect neighborhood -> broadcast Hello messages 
        helloMsg = HelloMsg()
        helloMsg.ag_name = self.label
        helloMsg.ag_addr = self.ns
        helloMsg.mission_id = missionID
        helloMsg.task_labels = self.treeDict[missionID].tasks.keys()
        helloMsg.neighbors = self.neighbors[missionID].keys()
        self.broadcastHelloPub.publish(helloMsg)
        
        # wait for incoming Hello messages from other agents
        rospy.sleep(2.0)
        
        self.waitTaskAssessmentFlag[missionID] = True
        result = self.mission_assess_request(missionID)
        if result == False:
            self.missionStatus[missionID] = "abort"
            self.abort_mission(missionID, False)
            return
        while self.waitTaskAssessmentFlag[missionID]:
            rospy.sleep(0.1)
        
        if self.check_failed(missionID, set()):
            return
        
        # get unscheduled task outcomes from scheduler
        self.get_unscheduled_task_outcome_ev(missionID)
        
        self.missionStatus[missionID] = "started_coordination"
        self.wait_for_neighborhood_state(missionID, "started_coordination")
        if self.check_failed(missionID, set()):
            return
        '''
        rospy.wait_for_service('preprocess_mission_structure')
        try:
            preprocess = rospy.ServiceProxy('preprocess_mission_structure', ReassessMissionTasks)
            result = preprocess(missionID, [], "")
            if result.done == 0:
                print "to remove"
                print result.to_remove
                for task in result.to_remove:
                    self.remove_task(missionID, task)
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
        raw_input("..")
        '''
        rospy.sleep(0.2)
        #print "PARENT CHILD CR"
        self.detect_parent_children_CRs(self.treeDict[missionID].rootTask[0], missionID)
        #print "................."
        missingNonLocalTasks = self.assess_non_local_tasks(missionID)
        if self.check_failed(missionID, set()):
            return
        
        if len(missingNonLocalTasks) > 0:
            print "\n ---- ERROR ---- \n"
            print " can't schedule mission, agents performing tasks " + str(list(missingNonLocalTasks)) + " are missing!\n"
            #TODO -- abort mission in this case??
            self.initialize_mission_rescheduling(missionID)
            return
        
        self.missionStatus[missionID] = "initial_task_assessment"
        self.wait_for_neighborhood_state(missionID, "initial_task_assessment")
        if self.check_failed(missionID, set()):
            return
        
        rospy.sleep(0.2)
        
        # check for complex mission
        if "q_max_all" in self.treeDict[missionID].tasks[rootTask].qaf_local and self.treeDict[missionID].tasks[rootTask].qaf in ["q_sum_all", "q_sum"]:
            self.waitComplexRedundancy[missionID] = True
            agents = self.handle_mission_redundancy(missionID, True)
            if agents == -1:
                self.missionStatus[missionID] = "abort"
                self.abort_mission(missionID, False)
                return
            self.waitComplexRedundancy[missionID] = False
        
        self.update_neighborhood_info(missionID)
        
        result = self.mission_reassess_request(missionID)
        if result == False:
            self.missionStatus[missionID] = "abort"
            self.abort_mission(missionID, False)
            return
        
        # get unscheduled task outcomes from scheduler
        self.get_unscheduled_task_outcome_ev(missionID)
        if self.check_failed(missionID, set()):
            return

        self.missionStatus[missionID] = "resolved_complex_redundancy"
        self.wait_for_neighborhood_state(missionID, "resolved_complex_redundancy")
        if self.check_failed(missionID, set()):
            return
        
        rospy.sleep(0.2)
        
        self.update_neighborhood_info(missionID)
        
        # get unscheduled task outcomes from scheduler
        self.get_unscheduled_task_outcome_ev(missionID)
        if self.check_failed(missionID, set()):
            return

        self.missionStatus[missionID] = "ready_to_schedule"
        self.wait_for_neighborhood_state(missionID, "ready_to_schedule")
        #print "PARENT CHILD CRs"
        self.parentChildCRs[missionID] = {}
        self.detect_parent_children_CRs(self.treeDict[missionID].rootTask[0], missionID)
        missingNonLocalTasks = self.assess_non_local_tasks(missionID)
        if self.check_failed(missionID, set()):
            return

        if len(missingNonLocalTasks) > 0:
            print "\n ---- ERROR ---- \n"
            print " can't schedule mission, agents performing tasks " + str(list(missingNonLocalTasks)) + " are missing!\n"
            self.initialize_mission_rescheduling(missionID)
            return

        rospy.sleep(0.1)
        result = self.reschedule_request(missionID)
        
        if result == -1:
            self.missionStatus[missionID] = "abort"
            self.abort_mission(missionID, False)
            return
        if result == -2:
            self.missionStatus[missionID] = "abort"
            self.abort_mission(missionID, False)
            return
        if result == -3:
            self.missionStatus[missionID] = "no_schedule"
            self.abort_mission(missionID, False)
            return
        
        self.missionStatus[missionID] = "initial_schedule"
        self.wait_for_neighborhood_state(missionID, "initial_schedule")
        if self.check_failed(missionID, set()):
            return

        self.update_neighborhood_info(missionID)
        if self.check_failed(missionID, set()):
            return

        # detect redundant tasks
        #print self.completedTasks[missionID]
        self.waitRedundant[missionID] = set()
        self.detect_redundant_tasks(self.treeDictScheduled[missionID].rootTask[0], missionID)
        rospy.sleep(0.2)
        
        missingNonLocalTasks = []
        for cr in self.parentChildCRs[missionID].values():
            if cr.count == 0 and cr.isMandatory:
                missingNonLocalTasks.append(cr.taskTo)
                
        if len(missingNonLocalTasks) > 0:
            print "\n ---- ERROR ---- \n"
            print " can't schedule mission, agents performing tasks " + str(list(missingNonLocalTasks)) + " are missing!\n"
            self.initialize_mission_rescheduling(missionID)
            return

        failed = self.communicate_redundancy(missionID)   
        if self.check_failed(missionID, failed):
            return

        recheck = False
        timeout = time.time() + 0.5
        while len(self.waitRedundant[missionID]) != 0:
            if time.time() > timeout:
                recheck = True
        
            if recheck:
                recheck = False
                failed = self.update_neighborhood_info(missionID)
                if self.check_failed(missionID, failed & self.waitRedundant[missionID]):
                    return
                timeout = time.time() + 0.5
        
        # if agent aborted mission
        if self.check_failed(missionID, set()):
            return
        
        self.missionStatus[missionID] = "removed_redundant"
        self.wait_for_neighborhood_state(missionID, "removed_redundant")

        # update -> some agents dismissed mission
        self.update_neighborhood_info(missionID)
        if self.check_failed(missionID, set()):
            return

        rospy.sleep(0.1)

        self.parentChildCRs[missionID] = {}
        self.detect_parent_children_CRs(self.treeDict[missionID].rootTask[0], missionID)

        missingNonLocalTasks = self.assess_non_local_tasks(missionID)
        if self.check_failed(missionID, set()):
            return

        if len(missingNonLocalTasks) > 0:
            print "\n ---- ERROR ---- \n"
            print " can't schedule mission, agents performing tasks " + str(list(missingNonLocalTasks)) + " are missing!\n"
            self.initialize_mission_rescheduling(missionID)
            return

        result = self.reschedule_request(missionID)

        if result == -1:
            self.missionStatus[missionID] = "abort"
            self.abort_mission(missionID, False)
            return
        if result == -2:
            self.missionStatus[missionID] = "abort"
            self.initialize_mission_rescheduling(missionID)
            return
        if result == -3:
            self.missionStatus[missionID] = "no_schedule"
            self.abort_mission(missionID, False)
            return
        if self.check_failed(missionID, set()):
            return
        
        self.missionStatus[missionID] = "uncoordinated"
        failed = self.wait_for_neighborhood_state(missionID, "uncoordinated")
        if self.check_failed(missionID, set()):
            return
        
        rospy.sleep(0.1)
        
        self.update_neighborhood_info(missionID)
        if self.check_failed(missionID, set()):
            return
        
        print "\n ------\n uncoordinated schedule: "
        print self.bestSchedule[missionID]
        print " ------"

        subcultron = False
        if subcultron:
            self.schedPub.publish(msg)
        else:
            # override this part for subcultron simulation
            self.commitmentsLocal[missionID] = {}
            self.commitmentsNonLocal[missionID] = {}
            #print "precedence constraint CRs"
            self.detect_precedence_constraint_CRs(missionID)
            #print ""
    
            failed = self.coordinate_hard_constrained_tasks(missionID)
            if self.check_failed(missionID, failed):
                return
    
            for task in self.treeDict[missionID].tasks.values():
                if task.subtasks is None:
                    if task.nonLocal:
                        for neighbor in self.neighbors[missionID].keys():
                            if task.label in self.neighbors[missionID][neighbor].scheduledMethods or task.label in self.neighbors[missionID][neighbor].completedTasks:
                                try:
                                    rospy.wait_for_service(neighbor + 'register_feedback_request', self.serviceTimeout)
                                    request_feedback = rospy.ServiceProxy(neighbor + 'register_feedback_request', RegisterFeedbackRequest)
                                    request_feedback(missionID, rospy.get_namespace(), task.label)
                                except rospy.ServiceException, e:
                                    print "Service call failed: %s"%e
                                except rospy.ROSException, e:
                                    print "Service call failed: %s"%e
    
            self.hardConstrainedNeighbors[missionID][1] = set()
            self.missionStatus[missionID] = "ready_for_mission_execution"
            error = self.check_neighborhood_state(missionID, "ready_for_mission_execution")
            while error == -1:
                #print error
                if self.scheduleIsCoordinated[missionID] == False:
                    failed = self.coordinate_hard_constrained_tasks(missionID)
                    if self.check_failed(missionID, failed):
                        return
                    self.hardConstrainedNeighbors[missionID][1] = set()
                    self.missionStatus[missionID] = "ready_for_mission_execution"
                rospy.sleep(0.1)
                error = self.check_neighborhood_state(missionID, "ready_for_mission_execution")
            
            rospy.sleep(0.2)
    
            if error == -2:
                self.missionStatus[missionID] = "restart"
            if self.check_failed(missionID, failed):
                return
            
            print "\n ------\n final schedule: "
            print self.bestSchedule[missionID]
            print " ------"
            
            # initialize simulator
            self.simulator[missionID] = simulator.LightSimulator(self.treeDict[missionID])
            tasksToComplete = deepcopy(self.completedTasks[missionID])
            tasksToComplete.extend(self.scheduledMethods[missionID])
            tasksToComplete.extend(self.completedNonLocal[missionID])
    
            self.simulator[missionID].init_disablements(self.scheduledMethods[missionID], tasksToComplete)
            
        if self.send_schedule_ok_execute(missionID):
            self.missionStatus[missionID] = "executing"
        else:
            self.initialize_mission_rescheduling(missionID)
            return
            
        print "\n total time elapsed:  " + str(rospy.get_time() - self.startTime[missionID]) + "\n"
        
    def coordinate_hard_constrained_tasks(self, missionID):
        
        commitList = self.commitmentsLocal[missionID].keys()
        sendOKTo = set()
        
        while True: 
            [failed, communicatedTo] = self.communicate_precedence_constraint_CRs(missionID, commitList)
            if len(failed) > 0:
                return failed
            
            rospy.sleep(0.3)
            
            hardConstrained = communicatedTo | self.hardConstrainedNeighbors[missionID][1]
            sendOKTo = sendOKTo | hardConstrained 
            
            self.missionStatus[missionID] = "committed"
            failed = self.wait_for_neighborhood_state(missionID, "committed", list(hardConstrained))
            if len(failed) > 0:
                return failed
           
            rospy.sleep(0.3)
            
            result = self.reschedule_request(missionID)
            if result == False:
                self.initialize_mission_rescheduling(missionID)
                return
            if self.check_failed(missionID, set()):
                return
        
            commitList = self.check_local_commitments(missionID)
            if len(commitList) == 0:
                self.scheduleIsCoordinated[missionID] = True
            self.waitForScheduleOK[missionID] = deepcopy(hardConstrained)
            self.hardConstrainedNeighbors[missionID][1] = set()
            self.missionStatus[missionID] = "scheduled"
            failed = self.wait_for_neighborhood_state(missionID, "scheduled", list(hardConstrained))
            
            if len(failed) > 0:
                return failed
            
            rospy.sleep(0.3)
            
            if self.scheduleIsCoordinated[missionID]:
                rospy.sleep(0.2)
                self.missionStatus[missionID] = "coordinated"
                failed = set()
                print "DONE -- waiting for " + str(self.waitForScheduleOK[missionID])
                temp = list(sendOKTo)
                for neighbor in temp:
                    try:
                        rospy.wait_for_service(neighbor + 'schedule_ok', self.serviceTimeout)
                        sched_ok = rospy.ServiceProxy(neighbor + 'schedule_ok', ScheduleOK)
                        sched_ok(missionID, rospy.get_namespace())
                        sendOKTo.remove(neighbor)
                    except rospy.ServiceException, e:
                        print "Service call failed: %s"%e
                        failed.add(neighbor)
                        self.remove_neighbor(missionID, neighbor)
                    except rospy.ROSException, e:
                        print "Service call failed: %s"%e
                        failed.add(neighbor)
                        self.remove_neighbor(missionID, neighbor)
                        
                if len(failed) > 0:
                    return failed
                
                recheck_interval = 0.5
                timeout = time.time() + recheck_interval
                while (len(self.waitForScheduleOK[missionID]) > 0):
                    if self.scheduleIsCoordinated[missionID] == False:
                        break
                    
                    if time.time() > timeout:
                        failed = self.update_neighborhood_info(missionID)
                        if len(failed) > 0:
                            return failed
                        timeout = time.time() + recheck_interval
                        
                if self.scheduleIsCoordinated[missionID] == False:
                    continue
                
                return set()
            
    def check_failed(self, missionID, failed):
        
        if self.missionStatus[missionID] == "abort":
            self.abort_mission(missionID, False)
            return True
        
        if self.missionStatus[missionID] == "restart":
            self.abort_mission(missionID, True)
            return True
        
        if len(failed) > 0:
            self.missionStatus[missionID] = "failed"
            print " neighbors " + str(list(failed)) + " are not accessible!"
            self.initialize_mission_rescheduling(missionID)
            return True
        
    def initialize_mission_rescheduling(self, missionID):
        
        task = self.missions[missionID]
        criteria = self.criteriaFilename[missionID]
        
        print " initializing mission rescheduling ..."
        msg = MissionCtrlMsg()
        msg.type = "Restart"
        msg.ag_addr = self.ns
        msg.mission_id = missionID
        msg.root_task = ""
        msg.criteria = ""  
        
        self.missionCtrlMsgPub.publish(msg)
        
        # simulate mission abort and wait for all other agents to abort mission
        self.missionStatus[missionID] = "no_mission"
        self.wait_for_neighborhood_state(missionID, "no_mission")
        self.abort_mission(missionID, True)
        
        msg = MissionCtrlMsg()
        msg.type = "NewMission"
        msg.ag_addr = self.missionCaller[missionID]
        msg.mission_id = missionID
        msg.root_task = task
        msg.criteria = criteria   # reschedule with the same criteria
        
        self.missionCtrlMsgPub.publish(msg)
        
    def wait_for_neighborhood_state(self, missionID, status, neighborList = None):
        if neighborList is None:
            neighbors = deepcopy(self.neighbors[missionID].keys())
        else:
            neighbors = deepcopy(neighborList)
            
        failed = set()
        
        print "\n " + str(self.ns) + " waiting for neighborhood state " + status + " " + str(neighbors)

        while len(neighbors) > 0:
            # if the mission was aborted or restarted
            if self.missionStatus[missionID] != status:
                return set()
            
            for neighbor in neighbors:
                try:
                    rospy.wait_for_service(neighbor + 'mission_status', self.serviceTimeout)
                
                    mission_status = rospy.ServiceProxy(neighbor + 'mission_status', MissionStatus)
                    response = mission_status(missionID)
                    
                    if response.mission_status == "no_mission" and status != "no_mission":
                        failed.add(neighbor)
                        self.remove_neighbor(missionID, neighbor)
                        neighbors.remove(neighbor)
                    if response.mission_status == "executing":
                        print " WARNING : mission is already executing on some agents! "
                        self.missionStatus[missionID] = "abort"
                    else:
                        if response.mission_status == status:
                            neighbors.remove(neighbor)
                    
                except rospy.ServiceException, e:
                    print "Service call failed: %s"%e
                    failed.add(neighbor)
                    self.remove_neighbor(missionID, neighbor)
                    neighbors.remove(neighbor)
                except rospy.ROSException, e:
                    print "Service call failed: %s"%e
                    failed.add(neighbor)
                    self.remove_neighbor(missionID, neighbor)
                    neighbors.remove(neighbor)   
                    
        return failed
    
    def check_neighborhood_state(self, missionID, status):
       
        # if the mission was aborted or restarted
        if self.missionStatus[missionID] != status:
            return -1
        
        print "\n checking neighborhood state " + status + " " + str(self.neighbors[missionID].keys())
        
        for neighbor in self.neighbors[missionID].keys():
            try:
                rospy.wait_for_service(neighbor + 'mission_status', self.serviceTimeout)
            
                mission_status = rospy.ServiceProxy(neighbor + 'mission_status', MissionStatus)
                response = mission_status(missionID)
                
                if response.mission_status == "no_mission" and status != "no_mission":
                    self.remove_neighbor(missionID, neighbor)
                    return -2
                if response.mission_status == "executing":
                    print " WARNING : mission is already executing on some agents! "
                    self.missionStatus[missionID] = "abort"
                else:
                    if response.mission_status != status:
                        return -1
                
            except rospy.ServiceException, e:
                print "Service call failed: %s"%e
                self.remove_neighbor(missionID, neighbor)
                return -2
            except rospy.ROSException, e:
                print "Service call failed: %s"%e
                self.remove_neighbor(missionID, neighbor)
                return -2
                    
        return 0
                    
    def update_neighborhood_info(self, missionID, neighbors=None):
        
        failed = set()
        if neighbors is None:
            neighbors = self.neighbors[missionID].keys()
        
        for neighbor in neighbors:
            if self.missionStatus[missionID] == "abort":
                return set()
            
            try:
                rospy.wait_for_service(neighbor + 'mission_info', self.serviceTimeout)

                mission_info = rospy.ServiceProxy(neighbor + 'mission_info', MissionInfo)
                response = mission_info(missionID)
                
                if response.my_mission == False:
                    self.remove_neighbor(missionID, neighbor)
                    failed.add(neighbor)
                else:
                    self.neighbors[missionID][neighbor].completedTasks = response.completed_tasks
                    self.neighbors[missionID][neighbor].scheduledMethods = response.scheduled_methods
                    self.neighbors[missionID][neighbor].taskLabels = response.task_labels
                
            except rospy.ServiceException, e:
                print "Service call failed: %s"%e
                failed.add(neighbor)
                self.remove_neighbor(missionID, neighbor)
            except rospy.ROSException, e:
                print "Service call failed: %s"%e
                failed.add(neighbor)
                self.remove_neighbor(missionID, neighbor)
                
        return failed
    
    def mission_assess_request(self, missionID):
        
        while self.poseEstimate is None:
            print " [GPGP] waiting for position information"
            rospy.sleep(0.5)
            
        neighbor_address_temp = self.neighbors[missionID].keys()
        neighbor_address = []
        neighbor_label = []
        
        for item in neighbor_address_temp:
            neighbor_label.extend(self.neighbors[missionID][item].label)
            neighbor_address.extend([item] * len(self.neighbors[missionID][item].label))
            
        rospy.wait_for_service('assess_mission_tasks')
        try:
            mission_assess = rospy.ServiceProxy('assess_mission_tasks', AssessMissionTasks)
            result = mission_assess(missionID, neighbor_label, neighbor_address)
            # if result.done is False, there was an error and mission should be canceled
            return result.done
        
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
            
    def mission_reassess_request(self, missionID):
        
        while self.poseEstimate is None:
            print " [GPGP] waiting for position information"
            rospy.sleep(0.5)
            
        neighbor_address = self.neighbors[missionID].keys()
        neighbor_task_labels = []
        for neighbor in neighbor_address:
            neighbor_task_labels.append(self.neighbors[missionID][neighbor].taskLabels)
        
        rospy.wait_for_service('reassess_mission_tasks')
        try:
            mission_reassess = rospy.ServiceProxy('reassess_mission_tasks', ReassessMissionTasks)
            #TODO should be positionEstimete[missionID] -> position at the beginning of each mission
            result = mission_reassess(missionID, neighbor_address, json.dumps(neighbor_task_labels))
            # if result.done is False, there was an error and mission should be canceled
            #print result
            if result.done == True:
                for task in result.to_remove:
                    self.remove_task(missionID, task)
            return result.done
        
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
    
    def get_unscheduled_task_outcome_ev(self, missionID):
        '''
        Gets task outcome from scheduler. Outcome is calculated as average value of quality, 
        duration and cost for every possible alternative that completes the task.
        '''
        rospy.wait_for_service('task_outcome_ev')
        try:
            task_outcome = rospy.ServiceProxy('task_outcome_ev', TaskOutcomeEV)
            result = task_outcome(missionID, self.missions[missionID], self.criteriaFilename[missionID])
            
            ev = json.loads(result.json_task_outcome_ev)
            self.taskOutcomeEVNonSched[missionID] = {}
            for i in range(len(result.all_tasks)):
                task = result.all_tasks[i]
                self.taskOutcomeEVNonSched[missionID][task] = [ev[i][0], ev[i][1], ev[i][2]]
                
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
           
    def reschedule_request(self, missionID):
        
        startTime = 0

        rospy.wait_for_service('mission_earliest_start')
        
        try:
            earliest_start = rospy.ServiceProxy('mission_earliest_start', MissionEarliestStart)
            result = earliest_start(missionID)
                
            startTime = result.mission_earliest_start
            
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
        
        rospy.wait_for_service('reschedule')
        try:
            reschedule = rospy.ServiceProxy('reschedule', Reschedule)
            result = reschedule(missionID, self.missions[missionID], self.criteriaFilename[missionID], startTime)
            # check for errors
            if result.done != 0:
                return result.done
                
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e 
        
        sched = []
        self.taskEndTime[missionID] = {}
        
        for i in range(len(result.task_labels)):
            sched.append([])
            sched[-1].append(result.task_labels[i])
            sched[-1].append(result.start_times[i])
            sched[-1].append(result.end_times[i])

        self.bestSchedule[missionID] = sched
        self.completedTasks[missionID] = result.completed_tasks

        for item in self.treeDict[missionID].homogeneousTasks:
            if set(self.treeDict[missionID].tasks[item].subtasks).issubset(set(result.completed_tasks)):
                self.completedTasks[missionID].append(item)

        self.completedNonLocal[missionID] = result.non_local_tasks
        self.scheduledMethods[missionID] = result.task_labels

        while "slack" in self.scheduledMethods[missionID]:
            self.scheduledMethods[missionID].remove("slack")
        
        for task in result.non_local_tasks:
            self.parentChildCRs[missionID][task].isMandatory = True

        # simulation of schedule updates treeDictScheduled[missionID], taskOutcome[missionID] 
        # and bestSchedule[missionID] - potentially different duration
        self.simulate_schedule(missionID, result.non_local_tasks)
        
        # get new task schedule times from executor (with respect to task ESTs)
        self.adjust_schedule_times_request(missionID)
        
        self.calc_task_end_times(missionID)

        print "-----------"
        print self.bestSchedule[missionID]
        print "-----------"
        return True
        
    def simulate_schedule(self, missionID, nonLocal):
        ''' 
        Simulate schedule execution to activate soft IR effects. Simulator also updates task outcomes.
        '''
        mySim = simulator.Simulator(self.treeDict[missionID])
        
        self.bestSchedule[missionID] = mySim.execute_schedule(self.bestSchedule[missionID], nonLocal, self.bestSchedule[missionID][0][1])
        
        self.treeDictScheduled[missionID] = mySim.taemsTree
        self.taskOutcome[missionID] = mySim.taskOutcome
        
        # tasks coupled for agent homogeneity
        for item in self.treeDict[missionID].homogeneousTasks:
            mySim.completedTasks.append(item)
            self.taskOutcome[item] = mySim.calc_QDC(item)
        
    def adjust_schedule_times_request(self, missionID):
        '''
        Requests schedule times adjustment from executor -- based on mission schedule, executor inserts it into agent's
        execution schedule and returns new task start and end times.
        '''
        tasks = []
        start = []
        end = []
        for item in self.bestSchedule[missionID]:
            if item[0] == "slack":
                continue
            tasks.append(item[0])
            start.append(item[1])
            end.append(item[2])
        
        constrained = []
        EST = []
        if len(self.commitmentsNonLocal[missionID]) > 0:
            for item in self.commitmentsNonLocal[missionID].values():
                constrained.extend(item.constrainedMethods)
                EST.extend([item.time] * len(item.constrainedMethods))
            
        rospy.wait_for_service('adjust_schedule_times')
        try:
            adjust_sched = rospy.ServiceProxy('adjust_schedule_times', AdjustScheduleTimes)
            
            result = adjust_sched(missionID, tasks, start, end, constrained, EST)
            
            schedule = []
            for i in range(len(result.task_labels)):
                schedule.append([result.task_labels[i], result.start_times[i], result.end_times[i]])
            self.bestSchedule[missionID] = schedule
            
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e 
        
    def calc_task_end_times(self, missionID):
        
        mySim = simulator.Simulator(self.treeDict[missionID])

        for task in self.completedNonLocal[missionID]:
            mySim.execute_non_local_task(task)

        mySim.calc_task_end_times(self.bestSchedule[missionID])
        
        self.taskEndTime[missionID] = mySim.taskEndTime
        
        # tasks coupled for agent homogeneity
        for item in self.treeDict[missionID].homogeneousTasks:
            if item in self.completedTasks[missionID]:
                time = 0
                for subtask in self.treeDict[missionID].tasks[item].subtasks:
                    if time == 0:
                        time = self.taskEndTime[missionID][subtask]
                    if self.treeDict[missionID].tasks[item].qaf in ['q_max']:
                        time = min(time, self.taskEndTime[missionID][subtask])
                    else:
                        time = max(time, self.taskEndTime[missionID][subtask])
                self.taskEndTime[missionID][item] = maxTime
        
    def remove_task(self, missionID, task):
        
        if task == self.missions[missionID]:
            self.missionStatus[missionID] = "abort"
            return
        
        if task not in self.treeDict[missionID].tasks.keys():
            return

        # DTC service
        rospy.wait_for_service('remove_task')
        try:
            remove_task = rospy.ServiceProxy('remove_task', RemoveTask)
            remove_task(missionID, task)
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
        # task_assessor service
        rospy.wait_for_service('assessor_remove_task')
        try:
            remove_task = rospy.ServiceProxy('assessor_remove_task', RemoveTask)
            remove_task(missionID, task)
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
        self.treeDict[missionID].removeTaskAndSubtasks(task)
        if missionID in self.treeDictScheduled.keys():
            self.treeDictScheduled[missionID].removeTaskAndSubtasks(task)
        '''
        if self.treeDictScheduled[missionID].tasks[task].subtasks is None:
            if task in self.scheduledMethods[missionID]:
                self.scheduledMethods[missionID].remove(task)
            return
        else:
            for subtask in self.treeDictScheduled[missionID].tasks[task].subtasks:
                self.remove_task(missionID, subtask)
        '''
                
    def handle_mission_redundancy(self, missionID, allMustBeScheduled):
        '''
        Handles complex mission redundancy. Problem is represented as operation research assignment problem and
        solved using branch and bound algorithm.
        '''
        subMissions = self.treeDict[missionID].tasks[self.missions[missionID]].subtasks
        redundantMissions = {}
        agents = [self.ns]

        #TODO -> for now it is hardcoded -- change to be flexible
        if "UGV_P" in self.label or "UGV_O" in self.label:
            labels = ["UGV_P", "UGV_O"]
        else:
            labels = ["UAV_Qc", "UAV_Qh"]
        
        for mission in subMissions:
            for agent in self.neighbors[missionID].values():
                if len(set(agent.label) - set(labels)) > 0:
                    continue
                if mission in agent.taskLabels:
                    if mission not in redundantMissions.keys():    
                        redundantMissions[mission] = [agent.address]
                    else:
                        redundantMissions[mission].append(agent.address)
                    if agent.address not in agents:
                        agents.append(agent.address)
            if mission in self.treeDict[missionID].tasks.keys():
                if self.treeDict[missionID].tasks[mission].subtasks is None:
                    if self.treeDict[missionID].tasks[mission].nonLocal:
                        continue
                if mission not in redundantMissions.keys():    
                    redundantMissions[mission] = [self.ns]
                else:
                    redundantMissions[mission].append(self.ns)
            if mission not in redundantMissions.keys():
                print " [GPGP] ERROR: mission is not possible - not enough agents for at least one submission!"
                print "    detected at " + mission + "submission"
                return -1
        agents.sort()

        #print "redundant missions"
        #print redundantMissions
        #print ""
        if len(agents) < len(subMissions) and allMustBeScheduled:
            print " [GPGP] ERROR: mission is not possible - not enough agents for at least one submission!"
            print "    number of agents: " + str(len(agents)) + " number of submissions: " + str(len(subMissions))
            return -1
        
        matrix = np.matrix([[0] * len(subMissions)] * len(agents), dtype=np.float64)
        q = []
        d = []
        c = []
        qEV = []
        dEV = []
        cEV = []
        endTime = []
        for i in range(len(subMissions)):
            task = subMissions[i]
            for j in range(len(agents)):
                agent = agents[j]
                # check if agent was removed
                '''if agent in toRemove:
                    subMissions[task].remove(agent)'''
                if agent not in redundantMissions[task]:
                    continue
                matrix[j,i] = 1

                if agent == self.ns:
                    qEV.append(self.taskOutcomeEVNonSched[missionID][task][0])
                    dEV.append(self.taskOutcomeEVNonSched[missionID][task][1])
                    cEV.append(self.taskOutcomeEVNonSched[missionID][task][2])
                    missionEndTime = self.startTime[missionID] + dEV[-1]
               
                    q.append({qEV[-1] : 1.0})
                    d.append({dEV[-1] : 1.0})
                    c.append({cEV[-1] : 1.0})
                    endTime.append(missionEndTime)
                else:
                    try:
                        rospy.wait_for_service(agent + 'task_info', self.serviceTimeout)
                        
                        task_info = rospy.ServiceProxy(agent + 'task_info', TaskInfo)
                        response = task_info(missionID, "non-sched", self.ns, task)
                        
                        if response.my_mission == False:
                            matrix[j,i] = 0
                        else:
                            qEV.append(response.outcome_ev[0])
                            dEV.append(response.outcome_ev[1])
                            cEV.append(response.outcome_ev[2])
                            missionEndTime = response.end_time
                            
                            q.append({qEV[-1] : 1.0})
                            d.append({dEV[-1] : 1.0})
                            c.append({cEV[-1] : 1.0})
                        
                            endTime.append(missionEndTime)
                
                    except rospy.ServiceException, e:
                        print "Service call failed: %s"%e
                        self.remove_neighbor(missionID, agent)
                    except rospy.ROSException, e:
                        print "Service call failed: %s"%e
                        self.remove_neighbor(missionID, agent)
            
        ratings = self.criteria[missionID].evaluate(q, d, c, qEV, dEV, cEV)  
        # rating based on quality of task's execution
        maxRating = max(ratings)
        minRating = min(ratings)
        if maxRating == minRating:
            maxRating = 1
            minRating = 0
        r1 = []
        for i in range(len(ratings)):
            r1.append((ratings[i] - minRating) / (maxRating - minRating))
        
        # rating based on time of task's execution    
        maxTime = max(endTime)
        minTime = min(endTime)
        if maxTime == minTime:
            maxTime = 1
            minTime = 0
        r2 = []
        for i in range(len(endTime)):
            r2.append((maxTime - endTime[i]) / (maxTime - minTime))
        
        totalRating = []
        for i in range(len(ratings)):
            totalRating.append(r1[i] * 0.7 + r2[i] * 0.3)   
        
        k = 0
        for i in range(len(subMissions)):
            for j in range(len(agents)):
                if matrix.item(j,i) == 1:
                    matrix[j,i] = totalRating[k]
                    k += 1
   
        missionAssignment = BranchAndBoundOptimizer.optimize(matrix)
        myMission = missionAssignment[agents.index(self.ns)]
        #print "mission assignments"
        #print missionAssignment
        #print agents
        #print "..................................."
        # if a mission is assigned to this agent
        if myMission < len(subMissions):
            # remove all other missions, their subtasks and assigned mission subtasks
            toRemove = deepcopy(subMissions) 
            toRemove.remove(subMissions[myMission])
            for mission in toRemove:
                self.remove_task(missionID, mission)
        else:
            return -1
        
        agents.remove(self.ns)
        return agents
                     
    def detect_redundant_tasks(self, task, missionID):

        tree = self.treeDictScheduled[missionID]
        if task not in tree.tasks.keys(): # or task not in self.completedTasks[missionID]:
            return
        if tree.tasks[task].subtasks is None:
            if tree.tasks[task].nonLocal:
                return 
        
        if tree.tasks[task].type == "homogeneous":  
            done = False
            metaTask = None
            
            for item in self.treeDict[missionID].homogeneousTasks:
                if task in self.treeDict[missionID].tasks[item].subtasks and item in self.completedTasks[missionID]:
                    #print "task in meta"
                    if self.treeDict[missionID].tasks[item].subtasks[0] != task:
                        #print "task is not first " + task + "   " + item
                        # to ensure that each meta task is detected as redundant only once
                        done = True
                    metaTask = item
                    break
            
            if done:
                return
            
            if metaTask is not None:
                task = metaTask
            #print "TASK: ---- " + task
            responsibleAgent = self.ns   
            redundant = False
            for neighbor in self.neighbors[missionID].keys():
                if len(set(self.neighbors[missionID][neighbor].label) & set(self.label)) == 0:
                    continue
                # if a neighbor scheduled the same method
                if task in self.neighbors[missionID][neighbor].completedTasks:
                    redundant = True
                    if task not in self.redundantTasks[missionID].keys():
                        self.redundantTasks[missionID][task] = []
                    self.redundantTasks[missionID][task].append(neighbor)
                    #print task + "   " + neighbor
                    if neighbor < responsibleAgent:
                        responsibleAgent = neighbor
            if not redundant:
                print "not redundant"
                return

            self.waitRedundant[missionID].add(responsibleAgent)
            if responsibleAgent == self.ns:
                self.responsibleFor[missionID].add(task)
                
            return  
        
        elif tree.tasks[task].type == "heterogeneous":
            for subtask in tree.tasks[task].subtasks:
                self.detect_redundant_tasks(subtask, missionID)
                
    def communicate_redundancy(self, missionID):
        
        #print "RESPONSIBLE:  " + str(self.responsibleFor[missionID])
        assignedTasks = []
        for task in self.responsibleFor[missionID]:
            if task in assignedTasks:
                continue
            while True:
                failed = set()
                agents = []
                q = []
                d = []
                c = []
                qEV = []
                dEV = []
                cEV = []
                endTime = []
                for neighbor in self.redundantTasks[missionID][task]:
                    try:
                        rospy.wait_for_service(neighbor + 'task_info', self.serviceTimeout)
                        
                        redundant_serv = rospy.ServiceProxy(neighbor + 'task_info', TaskInfo)
                        response = redundant_serv(missionID, "sched", self.ns, task)
                        
                        if response.my_mission == False:
                            self.remove_neighbor(missionID, neighbor)
                        elif response.my_task:
                            agents.append(neighbor)
                            
                            q.append(json.loads(response.json_outcome_q)) 
                            d.append(json.loads(response.json_outcome_d))
                            c.append(json.loads(response.json_outcome_c))
                            
                            qEV.append(response.outcome_ev[0])
                            dEV.append(response.outcome_ev[1])
                            cEV.append(response.outcome_ev[2])
                            
                            endTime.append(response.end_time)
                        
                    except rospy.ServiceException, e:
                        print "Service call failed: %s"%e
                        self.remove_neighbor(missionID, neighbor)
                    except rospy.ROSException, e:
                        print "Service call failed: %s"%e
                        self.remove_neighbor(missionID, neighbor)
                      
                agents.append(self.ns)
                #print "agents"
                #print agents
                q.append(self.taskOutcome[missionID][task][0])
                d.append(self.taskOutcome[missionID][task][1])
                c.append(self.taskOutcome[missionID][task][2])
                
                qEV.append(helper_functions.calcExpectedValue(self.taskOutcome[missionID][task][0]))
                dEV.append(helper_functions.calcExpectedValue(self.taskOutcome[missionID][task][1]))
                cEV.append(helper_functions.calcExpectedValue(self.taskOutcome[missionID][task][2]))
                
                endTime.append(self.taskEndTime[missionID][task])
                ratings = self.criteria[missionID].evaluate(q, d, c, qEV, dEV, cEV)   
  
                # rating based on quality of task's execution
                maxRating = max(ratings)
                minRating = min(ratings)
                r1 = []
                for i in range(len(ratings)):
                    if maxRating == minRating:
                        r1.append(ratings[i] - minRating)
                    else:
                        r1.append((ratings[i] - minRating) / (maxRating - minRating))
                
                # rating based on time of task's execution    
                maxTime = max(endTime)
                minTime = min(endTime)
                r2 = []
                for i in range(len(endTime)):
                    if maxTime == minTime:
                        r2.append(maxTime - endTime[i])
                    else:
                        r2.append((maxTime - endTime[i]) / (maxTime - minTime))
                
                totalRating = []
                for i in range(len(ratings)):
                    totalRating.append(r1[i] * 0.7 + r2[i] * 0.3)

                bestAgent = agents[totalRating.index(max(totalRating))]
                print task
                print "best:  "  + bestAgent
                # send 
                for neighbor in agents:
                    try:
                        rospy.wait_for_service(neighbor + 'resolve_redundant', self.serviceTimeout)
                    
                        solve_redundancy = rospy.ServiceProxy(neighbor + 'resolve_redundant', RedundancySolution)
                        solve_redundancy(self.ns, missionID, task, bestAgent)
                    except rospy.ServiceException, e:
                        print "Service call failed: %s"%e
                        if neighbor == bestAgent:
                            failed.add(neighbor)
                        self.remove_neighbor(missionID, neighbor)
                    except rospy.ROSException, e:
                        print "Service call failed: %s"%e
                        if neighbor == bestAgent:
                            failed.add(neighbor)
                        self.remove_neighbor(missionID, neighbor)
                
                if len(failed) > 0:
                    continue
                else:
                    break
       
        return set()
                
    def detect_parent_children_CRs(self, task, missionID):   
        '''recursive method that detects children-parent relationships
        '''
        tree = self.treeDict[missionID]
        
        # method
        if tree.tasks[task].subtasks == None:
            return
        
        for subtask in tree.tasks[task].subtasks:
            createCR = subtask not in tree.tasks.keys()
            if subtask in tree.tasks.keys():
                if type(tree.tasks[subtask]) is taems.Method:
                    if tree.tasks[subtask].nonLocal:
                        createCR = True
            if createCR:
                self.parentChildCRs[missionID][subtask] = CoordinationRelationshipParentChild(task, subtask, False)
                #print "subtask " + subtask
                for neighbor in self.neighbors[missionID]:
                    if subtask in self.neighbors[missionID][neighbor].taskLabels:
                        self.parentChildCRs[missionID][subtask].add_agent_to(neighbor, self.neighbors[missionID][neighbor].address)
                        #print neighbor
            else:
                self.detect_parent_children_CRs(subtask, missionID)
    
    def assess_non_local_tasks(self, missionID):
        
        missingNonLocalTasks = set()
        
        for task in self.parentChildCRs[missionID].keys():
            qEV = []
            dEV = []
            cEV = []
            taskIsMandatory = self.parentChildCRs[missionID][task].isMandatory
            i = 0

            while i < self.parentChildCRs[missionID][task].count:
                try:    
                    rospy.wait_for_service(self.parentChildCRs[missionID][task].agentToAddr[i] + 'task_info', self.serviceTimeout)
                
                    task_info = rospy.ServiceProxy(self.parentChildCRs[missionID][task].agentToAddr[i] + 'task_info', TaskInfo)
                    response = task_info(missionID, "non-sched", self.ns, task)
                    
                    if response.my_mission == False:
                        self.remove_neighbor(missionID, self.parentChildCRs[missionID][task].agentToAddr[i])
                        i -= 1
                    elif response.my_task == False:
                        self.parentChildCRs[missionID][task].remove_agent_to(self.parentChildCRs[missionID][task].agentToAddr[i])
                        i -= 1
                    else:
                        qEV.append(response.outcome_ev[0]) 
                        dEV.append(response.outcome_ev[1])
                        cEV.append(response.outcome_ev[2])
                    
                except rospy.ServiceException, e:
                    print "Service call failed: %s"%e
                    self.remove_neighbor(missionID, self.parentChildCRs[missionID][task].agentToAddr[i])
                    i -= 1
                except rospy.ROSException, e:
                    print "Service call failed: %s"%e
                    self.remove_neighbor(missionID, self.parentChildCRs[missionID][task].agentToAddr[i])
                    i -= 1
                    
                i += 1

            if len(qEV) > 0:
                self.add_non_local_task(missionID, task, self.parentChildCRs[missionID][task].taskFrom, [sum(qEV) / len(qEV), sum(dEV) / len(dEV), sum(cEV) / len(cEV)])
            else:
                if task in self.treeDict[missionID].tasks.keys():
                    #print "$$$$$$$$$$$$ Missing non local task " + task
                    self.remove_task(missionID, task)
                if taskIsMandatory:
                    # no need to assess other tasks
                    break
        
        for cr in self.parentChildCRs[missionID].values():
            if cr.count == 0 and cr.isMandatory:
                missingNonLocalTasks.add(cr.taskTo)
                           
        return missingNonLocalTasks
            
    def add_non_local_task(self, missionID, taskLabel, supertask, outcomeEV):

        task = taems.Method()
        task.label = taskLabel
        task.supertasks = [supertask]
        q = {outcomeEV[0] : 1.0}
        d = {outcomeEV[1] : 1.0}
        c = {outcomeEV[2] : 1.0} 
        task.outcome = [q, d, c]
        # used for execution, has to be local for simulator to count it into parent tasks's completion
        task.nonLocal = True
        
        self.treeDict[missionID].tasks[taskLabel] = task
        
        rospy.wait_for_service('add_non_local_task')
        try:
            add_non_local = rospy.ServiceProxy('add_non_local_task', AddNonLocalTask)
            add_non_local(missionID, taskLabel, supertask, outcomeEV)
                
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e 
            
    def remove_non_local_task(self, missionID, task):
        
        if task in self.treeDict[missionID].tasks.keys():
            if self.treeDict[missionID].tasks[task].subtasks is None:
                if self.treeDict[missionID].tasks[task].nonLocal:
                    print "removing non local task:  " + task
                    self.treeDict[missionID].tasks.pop(task)
                    # TODO --> use remove_task function instead
                    # DTC service
                    rospy.wait_for_service('remove_task')
                    try:
                        remove_non_local = rospy.ServiceProxy('remove_task', RemoveTask)
                        remove_non_local(missionID, task)
                            
                    except rospy.ServiceException, e:
                        print "Service call failed: %s"%e 
                
    def detect_precedence_constraint_CRs(self, missionID):
        
        for ir in self.treeDictScheduled[missionID].IRs.values():
            #enables
            if ir.type == 0:
                if ir.From in self.completedTasks[missionID] or ir.From in self.scheduledMethods[missionID]:
                    if ir.From not in self.taskEndTime[missionID].keys():
                        continue
                    for neighbor in self.neighbors[missionID]:
                        if ir.To in self.neighbors[missionID][neighbor].completedTasks or ir.To in self.neighbors[missionID][neighbor].scheduledMethods:
                            self.hardConstrainedNeighbors[missionID][0].add(neighbor)
                            if ir.From not in self.commitmentsLocal[missionID].keys():
                                self.commitmentsLocal[missionID][ir.From] = LocalCommitment(self.ns, ir.From, neighbor, ir.To)
                            else:
                                self.commitmentsLocal[missionID][ir.From].add_task_to(neighbor, ir.To)     
                            #print [ir.From, ir.To, neighbor]         
            #disables
            if ir.type == 1:
                if ir.To in self.completedTasks[missionID] or ir.To in self.scheduledMethods[missionID]:
                    if ir.To not in self.taskEndTime[missionID].keys():
                        continue
                    for neighbor in self.neighbors[missionID]:
                        if ir.From in self.neighbors[missionID][neighbor].compleredTasks or ir.From in self.neighbors[missionID][neighbor].scheduledMethods:
                            self.hardConstrainedNeighbors[missionID][0].add(neighbor)
                            if ir.To not in self.commitmentsLocal[missionID].keys():
                                self.commitmentsLocal[missionID][ir.To] = LocalCommitment(self.ns, ir.To, neighbor, ir.From)
                            else:
                                self.commitmentsLocal[missionID][ir.To].add_task_to(neighbor, ir.From)
                            #print [ir.To, ir.From, neighbor]

    def communicate_precedence_constraint_CRs(self, missionID, tasks):
        print "communicate precedence constraints " + str(tasks)
        failed = set()
        communicatedTo = set()
        
        for task in tasks:
            
            if task not in self.commitmentsLocal[missionID].keys():
                continue
            
            time = self.taskEndTime[missionID][task]
            result = True
            self.commitmentsLocal[missionID][task].time = time
            
            if result == True:
                i = 0
                while i < self.commitmentsLocal[missionID][task].count:
                    print task + "   " + self.commitmentsLocal[missionID][task].agentToAddr[i] + "   " +  self.commitmentsLocal[missionID][task].taskTo[i] + "   " + str(time)
                    try:
                        rospy.wait_for_service(self.commitmentsLocal[missionID][task].agentToAddr[i] + 'register_commitment', self.serviceTimeout)
                        register_commitment = rospy.ServiceProxy(self.commitmentsLocal[missionID][task].agentToAddr[i] + 'register_commitment', AddCommitmentNonLocal)
                        register_commitment(missionID, self.ns, task, self.commitmentsLocal[missionID][task].taskTo[i], time)
                        communicatedTo.add(self.commitmentsLocal[missionID][task].agentToAddr[i])
                    except rospy.ServiceException, e:
                        print "Service call failed: %s"%e
                        failed.add(self.commitmentsLocal[missionID][task].agentToAddr[i])
                        self.remove_neighbor(missionID, self.commitmentsLocal[missionID][task].agentToAddr[i])
                        i -= 1
                    except rospy.ROSException, e:
                        print "Service call failed: %s"%e
                        failed.add(self.commitmentsLocal[missionID][task].agentToAddr[i])
                        self.remove_neighbor(missionID, self.commitmentsLocal[missionID][task].agentToAddr[i])
                        i -= 1
                    i += 1 
                    if task not in self.commitmentsLocal[missionID].keys():
                        break
            else:
                print "Could not add local commitment for task " + task
        print "................."    
        return [failed, communicatedTo]    
         
    def check_local_commitments(self, missionID):
        
        toRecommit = []
        
        for task in self.commitmentsLocal[missionID].keys():
            if self.taskEndTime[missionID][task] > self.commitmentsLocal[missionID][task].time:
                toRecommit.append(task)
                
        return toRecommit
           
    def send_commitment_to_scheduler(self, missionID, commType, task, time, breakTime):
        
        rospy.wait_for_service('add_commitment')
        try:
            add_commitment = rospy.ServiceProxy('add_commitment', AddCommitmentLocal)
            result = add_commitment(missionID, commType, task, time, breakTime)
                
            return result.done
        
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e 
        
    def remove_neighbor(self, missionID, neighbor):
        
        if neighbor in self.neighbors[missionID].keys():
            self.neighbors[missionID].pop(neighbor)
        
        for task in self.parentChildCRs[missionID].keys():
            self.parentChildCRs[missionID][task].remove_agent_to(neighbor)
            if self.parentChildCRs[missionID][task].count == 0:
                self.remove_non_local_task(missionID, task) #TODO check this part -> could it be that there are other agents performing this task
            
        for commitment in self.commitmentsLocal[missionID].values():
            commitment.remove_agent_to(neighbor)
            if commitment.count == 0:
                self.commitmentsLocal[missionID].pop(commitment.taskFrom[0])
        
        for commitment in self.commitmentsNonLocal[missionID].values():
            commitment.remove_agent_from(neighbor)
            if commitment.count == 0:
                self.commitmentsNonLocal[missionID].pop(commitment.taskTo[0])
                    
        for task in self.redundantTasks[missionID].keys():
            for item in self.redundantTasks[missionID][task]:
                if item == neighbor:
                    self.redundantTasks[missionID][task].remove(item)
                    break
                
    def send_schedule_ok_execute(self, missionID):
        
        rospy.wait_for_service('schedule_ok_execute')
        try:
            send_ok = rospy.ServiceProxy('schedule_ok_execute', ScheduleOK)
            result = send_ok(missionID, "")
                
            return result.done
        
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e 
         
    # callback functions

    def msg_mission_ctrl_callback(self, msg):
        
        if msg.type == "NewMission":
            if msg.mission_id not in self.missions.keys():
                if msg.mission_id in self.missionStatus.keys():
                    if self.missionStatus[msg.mission_id] == "no_schedule":
                        return
                self.missionCaller[msg.mission_id] = msg.ag_addr
                self.missionType[msg.mission_id] = msg.mission_type
                self.start_new_mission(msg.mission_id, msg.root_task, msg.criteria)
                
        if msg.type == "Abort":
            if msg.ag_addr == self.ns:
                return
            
            if msg.mission_id in self.missions.keys():
                print " Got ABORT from " + msg.ag_addr + "mission " + msg.mission_id
                self.missionStatus[msg.mission_id] = "abort"
                
        if msg.type == "Restart":
            if msg.ag_addr == self.ns:
                return
            
            if msg.mission_id in self.missions.keys():
                print " Got RESTART from " + msg.ag_addr + "mission " + msg.mission_id
                self.missionStatus[msg.mission_id] = "restart"
                
        if msg.type == "Completed":
            if msg.ag_addr != self.ns:
                return
            
            if msg.mission_id in self.missions.keys():
                # send message control to agent's nodes
                msgNew = MissionCtrlMsg()
                msgNew.type = "Completed"
                msgNew.mission_id = msg.mission_id
                self.missionCtrlMsgLocalPub.publish(msgNew)
                self.missionStatus[msg.mission_id] = "completed"
                #self.abort_mission(msg.mission_id, False)
    
    def msg_hello_callback(self, msg):
        
        # if this agent is sender - do nothing
        if msg.ag_addr == self.ns:
            return
        
        if msg.mission_id not in self.missions.keys():
            # maybe the mission wasn't registered yet -> wait a little bit
            rospy.sleep(1.0)
            
        if msg.mission_id in self.missions.keys():
            if self.missionStatus[msg.mission_id] == "executing":
                try:
                    rospy.wait_for_service(msg.ag_addr + 'signal_abort')
                
                    signal_abort = rospy.ServiceProxy(msg.ag_addr + 'signal_abort', SignalMissionAbort)
                    signal_abort(msg.mission_id, self.ns)
                     
                except rospy.ServiceException, e:
                    print "Service call failed: %s"%e
                except rospy.ROSException, e:
                    print "Service call failed: %s"%e
                    
                return
                
            if msg.ag_addr not in self.neighbors.keys():
                
                self.neighbors[msg.mission_id][msg.ag_addr] = Agent(msg.ag_addr, msg.ag_name)
                self.neighbors[msg.mission_id][msg.ag_addr].taskLabels = msg.task_labels
                
            if self.missionStatus[msg.mission_id] != "start" and self.missionStatus[msg.mission_id] != "started_corrdination":
                self.initialize_mission_rescheduling(msg.mission_id)
        
    def update_task_structure_callback(self, msg):
        
        outcomes = json.loads(msg.json_outcome)
        
        for i in range(len(msg.task_label)):
            task = msg.task_label[i]
            outcome = [{},{},{}]
            for item in outcomes[i][0].keys():
                outcome[0][int(float(item))] = outcomes[i][0][item]
            for item in outcomes[i][1].keys():
                outcome[1][int(float(item))] = outcomes[i][1][item]
            for item in outcomes[i][2].keys():
                outcome[2][int(float(item))] = outcomes[i][2][item]
            self.treeDict[msg.mission_id].tasks[task].outcome = outcome
            
            self.treeDict[msg.mission_id].tasks[task].DurationEV = helper_functions.calcExpectedValue(outcome[1])
            self.treeDict[msg.mission_id].tasks[task].CostEV = helper_functions.calcExpectedValue(outcome[2])
            
        if msg.mission_id in self.treeDictScheduled.keys():
            self.simulate_schedule(msg.mission_id)
            
        self.waitTaskAssessmentFlag[msg.mission_id] = False
         
    # service functions 
    
    def register_abort_srv(self, req):
        
        print " Mission " + req.mission_id + " is already executing on agent " + req.ag_addr
        if req.mission_id in self.missions.keys():
            self.missionStatus[req.mission_id] = "abort"
        return []
        
    def task_info_srv(self, req):
        
        missionID = req.mission_id
        taskLabel = req.task_label
        
        while self.missionStatus[missionID] == "start":
            pass
        
        if self.missionStatus[missionID] in ["abort", "complete", "no_schedule", "no_mission"]:
            return [False, False, "", "", "", [], 0]

        if taskLabel not in self.treeDict[missionID].tasks.keys():
            return [True, False, "", "", "", [], 0]
        if self.treeDict[missionID].tasks[taskLabel].subtasks is None:
            if self.treeDict[missionID].tasks[taskLabel].nonLocal:
                return [True, False, "", "", "", [], 0]

        if req.type == "sched":
            outcome = [json.dumps(self.taskOutcome[missionID][taskLabel][0])]
            outcome.append(json.dumps(self.taskOutcome[missionID][taskLabel][1]))
            outcome.append(json.dumps(self.taskOutcome[missionID][taskLabel][2]))
            
            qualityEV = helper_functions.calcExpectedValue(self.taskOutcome[missionID][taskLabel][0])
            durationEV = helper_functions.calcExpectedValue(self.taskOutcome[missionID][taskLabel][1])
            costEV = helper_functions.calcExpectedValue(self.taskOutcome[missionID][taskLabel][2])
            
            return [True, True, outcome[0], outcome[1], outcome[2], [qualityEV, durationEV, costEV], self.taskEndTime[missionID][taskLabel]]
        else:
            if taskLabel in self.taskOutcomeEVNonSched[missionID].keys():
                outcome_ev = self.taskOutcomeEVNonSched[missionID][taskLabel]
                return [True, True, "", "", "", [outcome_ev[0], outcome_ev[1], outcome_ev[2]], self.startTime[missionID] + outcome_ev[1]]
            else:
                return [True, False, "", "", "", [], 0]

    def register_feedback_request_srv(self, req):

        if req.task_label not in self.feedbackRequestList[req.mission_id].keys():
            self.feedbackRequestList[req.mission_id][req.task_label] = LocalCommitment("", req.task_label, req.agent, "")
        else:
            if req.agent not in self.feedbackRequestList[req.mission_id][req.task_label].agentToAddr:
                self.feedbackRequestList[req.mission_id][req.task_label].add_task_to(req.agent, req.task_label)

        return []

    def mission_info_srv(self, req):
        
        missionID = req.mission_id
        
        if missionID in self.missions.keys() and self.missionStatus[missionID] not in ["abort", "no_schedule", "no_mission"]:
            while missionID in self.waitComplexRedundancy.keys():
                if self.waitComplexRedundancy[missionID]:
                    rospy.sleep(0.1)
                else:
                    break
            if missionID in self.missions.keys():
                if missionID not in self.completedTasks.keys():
                    return [True, [], [], self.treeDict[missionID].tasks.keys()]
                else:
                    tasks = deepcopy(self.treeDict[missionID].tasks.keys())
                    for task in self.treeDict[missionID].tasks.keys():
                        if self.treeDict[missionID].tasks[task].subtasks is None:
                            if self.treeDict[missionID].tasks[task].nonLocal:
                                tasks.remove(task)
                    return [True, self.completedTasks[missionID], self.scheduledMethods[missionID], self.treeDict[missionID].tasks.keys()]
         
        return [False, [], [], []]
    
    def mission_status_srv(self, req):
        
        if req.mission_id in self.missions.keys():
            return self.missionStatus[req.mission_id]
        
        return "no_mission"
    
    def resolve_redundant_srv(self, req):
        
        if req.mission_id not in self.missions.keys():
            return False

        if req.best_ag_addr != self.ns:
            self.remove_task(req.mission_id, req.task_label)
            # check if there are any items left in schedule
            subtasks = self.treeDictScheduled[req.mission_id].get_all_subtasks(req.task_label)
            remainingTasks = len(self.bestSchedule[req.mission_id])
            for i in range(len(self.bestSchedule[req.mission_id])):
                if self.bestSchedule[req.mission_id][i][0] in subtasks:
                    remainingTasks -= 1
            if remainingTasks == 0:
                self.missionStatus[req.mission_id] = "abort"
        
        self.waitRedundant[req.mission_id].remove(req.ag_addr)
        
        return True
    
    def add_commitment_non_local_srv(self, req):

        if req.ag_addr not in self.neighbors[req.mission_id]:
            return False

        if req.task_label_to not in self.commitmentsNonLocal[req.mission_id].keys():
            self.commitmentsNonLocal[req.mission_id][req.task_label_to] = NonLocalCommitment(req.ag_addr, req.task_label_from, self.ns, req.task_label_to, req.time)
            self.commitmentsNonLocal[req.mission_id][req.task_label_to].constrainedMethods = self.get_constrained_methods(req.mission_id, req.task_label_to)
        else:  
            self.commitmentsNonLocal[req.mission_id][req.task_label_to].add_task_from(req.ag_addr, req.task_label_from, req.time)
        self.hardConstrainedNeighbors[req.mission_id][1].add(req.ag_addr)
        result = self.send_commitment_to_scheduler(req.mission_id, "earliest_start_time", req.task_label_to, req.time, False)
        
        if self.scheduleIsCoordinated[req.mission_id] == True:
            self.scheduleIsCoordinated[req.mission_id] = False
        return result
    
    def get_constrained_methods(self, missionID, task):
        
        if task not in self.treeDict[missionID].tasks.keys():
            return []
        
        if self.treeDict[missionID].tasks[task].subtasks is None:
            return [task]
        
        methods = []
        for subtask in self.treeDict[missionID].tasks[task].subtasks:
            methods.extend(self.get_constrained_methods(missionID, subtask))
            
        return methods
    
    def register_schedule_ok_srv(self, req):
        
        if req.mission_id not in self.missions.keys():
            return False
        
        if req.mission_id in self.waitForScheduleOK.keys():
            if req.ag_addr in self.waitForScheduleOK[req.mission_id]:
                self.waitForScheduleOK[req.mission_id].remove(req.ag_addr)
        
        return True
    
    def register_executed_task_srv(self, req):
        
        #if self.missionStatus[req.mission_id] == "completed":
        #    return
        tasksToComplete = deepcopy(self.completedTasks[req.mission_id])
        tasksToComplete.extend(self.scheduledMethods[req.mission_id])
        tasksToComplete.extend(self.completedNonLocal[req.mission_id])

        while self.simulator[req.mission_id] is None:
            rospy.sleep(0.2)
        # if task exists, register its execution
        if req.task != "" and req.task != "slack":
            completedBefore = set(self.simulator[req.mission_id].completedTasks)

            if req.task in self.scheduledMethods[req.mission_id]:
                del self.bestSchedule[req.mission_id][0]

            self.simulator[req.mission_id].execute_task(req.task, tasksToComplete)
            completedAfter = set(self.simulator[req.mission_id].completedTasks)
            completedNew = completedAfter - completedBefore

            # communicate to other agents
            for task in completedNew:
                communicateTo = set()
                if task in self.commitmentsLocal[req.mission_id].keys():
                    for i in range(self.commitmentsLocal[req.mission_id][task].count):
                        communicateTo.add(self.commitmentsLocal[req.mission_id][task].agentToAddr[i])
                    self.commitmentsLocal[req.mission_id].pop(task)
                if task in self.feedbackRequestList[req.mission_id]:
                    communicateTo = communicateTo | set(self.feedbackRequestList[req.mission_id][task].agentToAddr)

                for neighbor in communicateTo:
                    try:
                        rospy.wait_for_service(neighbor + 'register_executed_task', self.serviceTimeout)
                        register_executed_task = rospy.ServiceProxy(neighbor + 'register_executed_task', ExecuteTask)
                        register_executed_task(req.mission_id, task, req.delay)
                    except rospy.ServiceException, e:
                        print "Service call failed: %s"%e
                    except rospy.ROSException, e:
                        print "Service call failed: %s"%e

            if req.task not in self.scheduledMethods[req.mission_id]:
                toRemove = set()
                for commitment in self.commitmentsNonLocal[req.mission_id].values():
                    if req.task in commitment.taskFrom:
                        commitment.remove_task_from(req.task)
                        if commitment.count == 0:
                            toRemove.add(commitment.taskTo[0])
                for task in toRemove:
                    self.commitmentsNonLocal[req.mission_id].pop(task)
                
        '''if req.delay != 0 and len(self.bestSchedule[req.mission_id]) > 0:
            while self.waitTaskAssessmentFlag[req.mission_id]:
                rospy.sleep(0.1)
            self.waitTaskAssessmentFlag[req.mission_id] = True
            schedStart = self.bestSchedule[req.mission_id][0][1]

            self.adjust_schedule_times_request(req.mission_id)
            delay = self.bestSchedule[req.mission_id][0][1] - schedStart
            
            for commitment in self.commitmentsLocal[req.mission_id].values():
                commitment.time += (delay + 0.1)
           
            rospy.wait_for_service('delay_schedule')
            try:
                delay_schedule = rospy.ServiceProxy('delay_schedule', DelaySchedule)
                if delay == 0:
                    delay_schedule(0)
                else:
                    delay_schedule(delay + 0.1)
                    
            except rospy.ServiceException, e:
                print "Service call failed: %s"%e
                     
            self.waitTaskAssessmentFlag[req.mission_id] = False'''
        
        enabled = self.simulator[req.mission_id].get_enabled_tasks(self.scheduledMethods[req.mission_id])

        disabledTasks = []
        for commitment in self.commitmentsNonLocal[req.mission_id].values():
            disabledTasks.extend(commitment.constrainedMethods)
            disabledTasks.extend(commitment.taskTo)
        disabledTasks = set(disabledTasks)
        enabled = enabled - disabledTasks
        msg = MissionEnabledTasks()
        msg.mission_id = req.mission_id
        msg.task_labels = enabled
        
        self.missionEnabledTasksPub.publish(msg)  
        return True
    
    def get_position_srv(self, srv):
        
        while self.poseEstimate is None:
            print "Waiting for agent position information"
            rospy.sleep(0.5)
        
        return self.poseEstimate.position

    def get_pose_srv(self, srv):
        
        while self.poseEstimate is None:
            print "Waiting for agent position information"
            rospy.sleep(0.5)
        
        return self.poseEstimate
    
    
class Agent(object):
    '''
    A class that represents peer agents included in coordination process.
    Attributes:
        address - agent's namespace
        label - a list of agent's labels (in taems structure)
        completedTasks
        scheduledMethods
    '''
    
    def __init__(self, address, label):
        self.address = address
        self.label = label
        self.completedTasks = []
        self.scheduledMethods = []
        self.taskLabels = []

class CoordinationRelationshipParentChild(object):
    
    def __init__(self, fromTask, toTask, isMandatory):
        self.taskFrom = fromTask
        self.taskTo = toTask
        self.agentToAddr = []
        self.agentToType = set()
        self.count = 0
        self.isMandatory = isMandatory
        
    def add_agent_to(self, toAgent, toAgentType):
        self.agentToAddr.append(toAgent)
        self.agentToType.add(toAgentType)
        self.count += 1
        
    def remove_agent_to(self, toAgent):
        while toAgent in self.agentToAddr:
            self.agentToAddr.remove(toAgent)
            self.count -= 1
    
    def remove_item_at(self, i):
        del self.agentToAddr[i]
        self.count -= 1
    
class Commitment(object):
    
    def __init__(self, fromAgent, fromTask, toAgent, toTask):
        self.taskFrom = [fromTask]
        self.taskTo = [toTask]
        self.agentToAddr = [toAgent]
        self.agentFromAddr = [fromAgent]
        self.count = 1
        self.time = -1
        
class LocalCommitment(Commitment):
    
    def __init__(self, fromAgent, fromTask, toAgent, toTask):
        super(LocalCommitment, self).__init__(fromAgent, fromTask, toAgent, toTask)
        
    def add_task_to(self, toAgent, toTask):
        self.agentToAddr.append(toAgent)
        self.taskTo.append(toTask)
        self.count += 1

    def set_time(self, time):
        self.time = time
        
    def remove_agent_to(self, toAgent):
        while toAgent in self.agentToAddr:
            index = self.agentToAddr.index(toAgent)
            del self.agentToAddr[index]
            del self.taskTo[index]
            self.count -= 1
        
class NonLocalCommitment(Commitment):
    
    def __init__(self, fromAgent, fromTask, toAgent, toTask, time):
        super(NonLocalCommitment, self).__init__(fromAgent, fromTask, toAgent, toTask)
        self.time = time
        self.constrainedMethods = []
        
    def contains(self, fromTask, fromAgent):
        for i in range(len(self.taskFrom)):
            if fromTask != self.taskFrom[i]:
                continue
            if fromAgent == self.agentFromAddr[i]:
                return True
        return False
        
    def add_task_from(self, fromAgent, fromTask, time):
        # check for existing commitment
        if self.contains(fromTask, fromAgent):
            self.set_time(time)
        else:
            self.agentFromAddr.append(fromAgent)
            self.taskFrom.append(fromTask)
            self.set_time(time)
            self.count += 1
        
    def remove_task_from(self, fromTask):
        index = self.taskFrom.index(fromTask)
        del self.agentFromAddr[index]
        del self.taskFrom[index]
        self.count -= 1
        
    def set_time(self, time):
        # earliest start time for a task is always the latest time of all ESTs
        if time == -1 or time > self.time:
            self.time = time
            
    def remove_agent_from(self, fromAgent):
        while fromAgent in self.agentFromAddr:
            index = self.agentFromAddr.index(fromAgent)
            del self.agentFromAddr[index]
            del self.taskFrom[index]
            self.count -= 1

if __name__ == "__main__":

    rospy.init_node("coordinator")
    
    try:
        coordination = GPGPCoordinator()
    except rospy.ROSInterruptException:
        pass
        
