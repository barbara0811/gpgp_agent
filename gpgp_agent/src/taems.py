
__author__ = "barbanas"

import io
import helper_functions
import copy 
from sys import maxint
from copy import deepcopy

class TaemsTree(object):
    '''TaemsTree is a class that represents the model of taems tree structure. It holds information
        about all elements that define a task structure.
        
        Attributes:
            agent: A string, label of agent which owns the structure
            agentLabels: A list of strings, labels of all agents included in the execution of methods in taems tree
            tasks: dictionary- task/method label -> TaskGroup/Method object
            rootTask: A list with one! element, label of root task
            methodLabels: list of labels of atomic methods
            resources: dictionary - resource label -> Resource object
            IRs: dictionary - IR label -> Interrelationship object
            activeIR: list of active interrelationships' labels
    '''

    def __init__(self):
        self.agent = ""  
        self.agentLabels = [] 
        self.tasks = {}
        self.rootTask = [] 
        self.methodLabels = [] 
        self.resources = {} 
        self.IRs = {} 
        self.mutuallyExclusiveTasks = []
        self.homogeneousTasks = []
        '''self.IRsKeyMethodFrom = {}'''
        '''self.activeIR = [] 
        self.softIR = []
        self.hardIR = []'''
        
    def get_all_subtasks(self, task):
        
        if task not in self.tasks.keys():
            return []
        
        if type(self.tasks[task]) is Method:
            return []
        
        allSubtasks = []
        for subtask in self.tasks[task].subtasks:
            if subtask in self.tasks.keys():
                allSubtasks.append(subtask)
            allSubtasks.extend(self.get_all_subtasks(subtask))
            
        return allSubtasks
    
    def removeTaskAndSubtasks(self, task):
        
        toRemove = self.get_all_subtasks(task)
        
        for item in toRemove:
            if task in self.tasks.keys():
                self.tasks.pop(item)
        
        if task in self.tasks.keys():
            self.tasks.pop(task)
        
    @staticmethod
    def merge(first, second):
        result = TaemsTree()
        
        result.agentLabels = copy.deepcopy(first.agentLabels)
        result.agentLabels.extend(second.agentLabels)
        
        result.methodLabels = copy.deepcopy(first.methodLabels)
        result.methodLabels.extend(second.methodLabels)
        
        result.rootTask = copy.deepcopy(first.rootTask)
        
        result.resources = copy.deepcopy(first.resources)
        result.resources.update(second.resources)
        
        result.IRs = copy.deepcopy(first.IRs)
        result.IRs.update(second.IRs)
        
        result.tasks = copy.deepcopy(first.tasks)
        for task in second.tasks.keys():
            if task in result.tasks.keys():
                if type(result.tasks[task]) is not Method:
                    result.tasks[task].subtasks = list(set(result.tasks[task].subtasks) | set(second.tasks[task].subtasks))
                if result.tasks[task].supertasks is not None:
                    result.tasks[task].supertasks = list(set(result.tasks[task].supertasks) | set(second.tasks[task].supertasks))
                result.tasks[task].earliestStartTime = max([result.tasks[task].earliestStartTime, second.tasks[task].earliestStartTime])
                result.tasks[task].deadline = min([result.tasks[task].deadline, second.tasks[task].deadline])
            else:
                result.tasks[task] = second.tasks[task]
        
        return result

class Agent():
    '''Agent is a class that models an agent which can execute methods in taems tree.
        
        Attributes:
            label: A string, unique string representation of an agent
    '''
    
    def __init__(self):
        self.label = ""       

class Node(object):
    '''An interface for an element of taems tree. Classes that inherit this interface are: Task and Resource.
    
    Attributes:
        label: A string that represents element's label (has to be unique among the elements of the same class)
        agent: A string, label of the agent who 'owns' the element (is responsible for it)
    '''

    def __init__(self):
        self.label = ""
        self.agent = []

class Task(Node):
    '''Task is an interface that represents a task (taskgroup and method) in taems tree structure.
    
    Attributes:
        subtasks: A list of strings, labels of its subtasks
        supertasks: A list of strings, labels of its supertasks
        earliestStartTime: A float number
        deadline: A float number
    '''

    def __init__(self):
        super(Task,self).__init__()
        self.subtasks = []    # doesn't exist for method
        self.supertasks = []  # optional for TaskGroup (if root task)
        self.earliestStartTime = None
        self.deadline = None
        self.type = "homogeneous"

class TaskGroup(Task):
    '''TaskGroup is a class that represents a task group in taems tree structure. 
        Task group is not executable.
    
    Attributes:
        qaf: A string, ENUM - has predefined values, label of qaf of the task group
    '''

    def __init__(self):
        super(TaskGroup, self).__init__()
        self.qaf = ""  
        self.qaf_local = "" 

class Method(Task):
    '''Method is a class that represents a method in taems tree structure. 
        Methods are executable.
    
    Attributes:
        outcome: A list of dictionaries.
                [0] quality_distribution
                [1] duration_distribution
                [2] cost_distribution
            -- each distribution is a dictionary : {value -> probability}
        QualityEV: A float number, expected value for quality of method's execution
        CostEV: A float number, expected value for cost of method's execution
        DurationEV: A float number, expected value for duration of method's execution
        startTime: A float number, start of method's execution
        endTime: A float number, end of method's execution
        accruedTime: A float number, time elapsed since the start of method's execution
        nonLocal: Boolean, true if method has to be executed by other agent
        isDisabled: Boolean, true if method is disabled (can't be executed)
    '''

    def __init__(self):
        super(Method, self).__init__()
        self.subtasks = None
        self.outcome = [{},{},{}]
        self.QualityEV = 0
        self.CostEV = 0
        self.DurationEV = 0
        #self.ProbQualityGreaterThanEV = 0
        #self.ProbCostLowerThanEV = 0
        #self.ProbDurationShorterThanEV = 0
        self.startTime = None
        self.endTime = None
        self.accruedTime = None
        self.nonLocal = False
        self.isDisabled = 0

    def calcExpectations(self):
        '''A method that calculates expected values for quality, duration and cost value distributions.
                It uses a helper function which calculates expected values of probability distributions 
                defined as: dictionary - {value -> probability}.
            Args:
                -
            Returns:
                -
        '''
        self.QualityEV = helper_functions.calcExpectedValue(self.outcome[0])
        self.DurationEV = helper_functions.calcExpectedValue(self.outcome[1])
        self.CostEV = helper_functions.calcExpectedValue(self.outcome[2])
        

class Resource(Node):
    '''Resource is an interface that represents resources in taems task structure.
    
    Attributes:
        state: A float value, current state of resource (quantity of available resource)
        depleted_at: A float value
        overloaded_at: A float value
        isSufficient: Boolean, true if depleted_at < state < overloaded_at
        type: An integer, 0 - consumable, 1 - non-consumable
    '''

    "type = consumable (0), non-consumable (1)"
    def __init__(self):
        super(Resource, self).__init__()
        self.state = 0
        self.depleted_at = 0
        self.overloaded_at = 0
        self.isSufficient = None
        self.type = -1

    def produce(self, amount, tree):
        '''Produces the given amount of resource and changes the sufficiency flag if needed.
        
            Args:
                amount: A float value
                tree: TaemsTree which resource belongs to
        '''
        self.checkSufficiency()
        wasInsuficcient = not self.isSufficient
        self.state += amount

        self.checkSufficiency()

        if self.isSufficient == False:
            self.activateLimits(tree)
        if self.isSufficient == True and wasInsuficcient:
            self.deactivateLimits(self, tree)
        
    def consume(self, amount, tree):
        '''Consumes the given amount of resource and changes the sufficiency flag if needed.
        
            Args:
                amount: A float value
                tree: TaemsTree which resource belongs to
            Returns:
                -
        '''
        self.checkSufficiency()
        wasInsuficcient = not self.isSufficient
        self.state -= amount

        self.checkSufficiency()

        if self.isSufficient == False:
            self.activateLimits(tree)
        if self.isSufficient == True and wasInsuficcient:
            self.deactivateLimits(tree)

    def checkSufficiency(self):
        '''Checks the current state of resource and sets sufficiency flag. 
            depleted_at < state < overloaded_at -> True
            else                                -> False
            
            Args:
                -
            Returns:
                -    
        '''
        if self.state > self.depleted_at and self.state < self.overloaded_at:
            self.isSufficient = True
        else:
            self.isSufficient = False

    def activateLimits(self, tree):
        '''Activates all limits interrelationships that have the insufficient 
            resource as source (from field). 
            
            Goes through all IR's of type limits (code 6) and checks to see if
            this resource is source. It then activates IRs that match criteria.
            
            Args:
                tree: TaemsTree instance
            Returns:
                -
        '''

        for ir in tree.IRs.values():
            # limits type is 6
            if ir.type == 6:
                if ir.From == self.label:
                    ir.activate(tree, 0)

    def deactivateLimits(self, tree):
        '''Deactivates all limits interrelationships that now have sufficient amount 
            of resource (from field). 
            
            Goes through all IR's of type limits (code 6) and checks to see if
            this resource is source. It then deactivates IRs that match criteria.
            
            Args:
                tree: TaemsTree instance
            Returns:
                -
        '''

        for ir in tree.IRs.values():
            # limits type is 6
            if ir.type == 6:
                if ir.From == self.label:
                    ir.deactivate(tree, 0)

class ConsumableResource(Resource):
    '''ConsumableResource is a class that represents consumable resources in taems task structure.
    '''

    def __init__(self):
        return super(ConsumableResource, self).__init__()
        self.type = 0

class NonConsumableResource(Resource):
    '''NonConsumableResource is a class that represents non-consumable resources in taems task structure.
        It has initial state to which it returns each time the action that changes its state finishes execution.
    '''

    def __init__(self):
        super(NonConsumableResource, self).__init__()
        self.initialState = 0
        self.type = 1

    def setInitalValue(self):
        '''Sets the resource's state to its initial value.
        '''
        self.state = self.initialState

class Interrelationship(object):
    '''An interface which represents interrelationships in taems tree structure.
    
    Attributes:
        label: A string, IR's label
        agent: A string, label of agent who "owns" the IR
        From: A string, label of the node that is the source of IR
        To: A string, label of the node that is affected by IR
        delay: A float number, value of time delayed before the effects of IR take place
        active: Boolean, True if IR is active
        type: An integer {0,1,2,3,4,5,6}, marks the type of IR
                0 - enables
                1 - disables
                2 - facilitates
                3 - hinders
                4 - produces
                5 - consumes
                6 - limits
                7 - child of
    '''
    
    def __init__(self):
        self.label = ""
        self.agent = ""
        self.From = ""
        self.To = ""
        self.delay = 0
        self.active = False
        self.type = -1

class IREnables(Interrelationship):
    '''A class which represents enables interrelationship.
    '''
    
    def __init__(self):
        super(IREnables, self).__init__()
        self.type = 0

    def activate(self, tree, time):
        '''Activates IR enables. 
        
            Sets the destination node isDisabled flag to false, modifies the destination's 
            earliest start time if needed, changes IR's state to active.
            
            Args:
                tree: TaemsTree
                time: A float number, current execution time
        '''
        tree.tasks[self.To].isDisabled -= 1 
        #if activation is delayed, set methods earliest start time
        if self.delay > 0:
            if tree.tasks[self.To].earliestStartTime < (time + self.delay) or tree.tasks[self.To].earliestStartTime is None:
                tree.tasks[self.To].earliestStartTime = time + self.delay

        self.active = True
        #tree.activeIR.append(self)

class IRDisables(Interrelationship):
    '''A class which represents disables interrelationship.
    '''
    
    def __init__(self):
        super(IRDisables, self).__init__()
        self.type = 1

    def activate(self, tree, time):
        '''Activates IR disables. 
        
            Sets the destination node isDisabled flag to true, modifies the destination's 
            deadline if needed, changes IR's state to active.
            
            Args:
                tree: TaemsTree
                time: A float number, current execution time
        '''
        tree.tasks[self.To].isDisabled += 1
        
        #if activation is delayed, set methods deadline
        if self.delay > 0:
            if tree.tasks[self.To].deadline > (time + self.delay) or tree.tasks[self.To].deadline is None:
                tree.tasks[self.To].deadline = time + self.delay
               
        self.active = True
        #tree.activeIR.append(self)

class IRFacilitates(Interrelationship):
    '''A class which represents facilitates interrelationship.
    
        Attributes:
            quality_power: Probability distribution of quality value which affects the destination node
            cost_power: Probability distribution of cost value which affects the destination node
            duration_power: Probability distribution of duration value which affects the destination node
            startTime: A float number
    '''
    
    def __init__(self):
        super(IRFacilitates, self).__init__()
        self.type = 2
        self.quality_power = {}
        self.cost_power = {}
        self.duration_power = {}
        self.startTime = None
        self.q_powerEV = -1
        self.d_powerEV = -1
        self.c_powerEV = -1

    def calcPowerEV(self):

        self.q_powerEV = helper_functions.calcExpectedValue(self.quality_power)
        self.d_powerEV = helper_functions.calcExpectedValue(self.duration_power)
        self.c_powerEV = helper_functions.calcExpectedValue(self.cost_power)

    def activate(self, tree, time):
        '''Activates IR facilitates. 
        
            Modifies the IR's start time, calculates quality, cost and duration
            expected values, modifies the destination's outcome.
            
            Args:
                tree: TaemsTree
                time: A float number, current execution time
        '''
        if self.delay > 0:
            if self.startTime is None or self.startTime < time + self.delay:
                self.startTime = time + self.delay
        else:
            if self.startTime is None or self.startTime > time:
                self.startTime = time

        helper_functions.mutiplyDistribution(tree.tasks[self.To].outcome[0], 1 + self.q_powerEV)
        helper_functions.mutiplyDistribution(tree.tasks[self.To].outcome[1], 1 - self.d_powerEV)
        helper_functions.mutiplyDistribution(tree.tasks[self.To].outcome[2], 1 - self.c_powerEV)
        
        self.active = True
        #tree.activeIR.append(self)
       
class IRHinders(Interrelationship):
    '''A class which represents hinders interrelationship.
    
        Attributes:
            quality_power: Probability distribution of quality value which affects the destination node
            cost_power: Probability distribution of cost value which affects the destination node
            duration_power: Probability distribution of duration value which affects the destination node
            startTime: A float number
    '''
    
    def __init__(self):
        super(IRHinders, self).__init__()
        self.type = 3
        self.quality_power = {}
        self.cost_power = {}
        self.duration_power = {}
        self.startTime = None
        self.q_powerEV = -1
        self.d_powerEV = -1
        self.c_powerEV = -1

    def calcPowerEV(self):

        self.q_powerEV = helper_functions.calcExpectedValue(self.quality_power)
        self.d_powerEV = helper_functions.calcExpectedValue(self.duration_power)
        self.c_powerEV = helper_functions.calcExpectedValue(self.cost_power)

    def activate(self, tree, time):
        '''Activates IR facilitates. 
        
            Modifies the IR's start time, calculates quality, cost and duration
            expected values, modifies the destination's outcome, activates the IR.
            
            Args:
                tree: TaemsTree
                time: A float number, current execution time
        '''
        if self.delay > 0:
            if self.startTime is None or self.startTime < time + self.delay:
                self.startTime = time + self.delay
        else:
            if self.startTime is None or self.startTime > time:
                self.startTime = time

        helper_functions.mutiplyDistribution(tree.tasks[self.To].outcome[0], 1 - self.q_powerEV)
        helper_functions.mutiplyDistribution(tree.tasks[self.To].outcome[1], 1 + self.d_powerEV)
        helper_functions.mutiplyDistribution(tree.tasks[self.To].outcome[2], 1 + self.c_powerEV)
        
        self.active = True
        #tree.activeIR.append(self)

class IRProduces(Interrelationship):
    '''A class which represents produces interrelationship.
    
        Attributes:
            model: A string, {"pet_time_unit","duration_independent"}
            produces: Probability distribution of quantity of resource IR produces
    '''
    
    def __init__(self):
        super(IRProduces, self).__init__()
        self.type = 4
        self.model = ""
        self.produces = {}

    def activate(self, tree):
        '''Activates IR produces. 
        
            Calculates the expected value of produced resource, produces the calculated 
            amount of resource, activates the IR.
            
            Args:
                tree: TaemsTree
        '''
        EVproduced = helper_functions.calcExpectedValue(self.produces)

        resource = tree.resources[self.To]
        if self.model == "per_time_unit":
            resource.produce(EVproduced * tree.tasks[self.From].DurationEV, tree)

        elif self.model == "duration_independent":
            resource.produce(EVproduced, tree)

        self.active = True
        #tree.activeIR.append(self)

class IRConsumes(Interrelationship):
    '''A class which represents consumes interrelationship.
    
        Attributes:
            model: A string, {"per_time_unit","duration_independent"}
            consumes: Probability distribution of quantity of resource IR consumes
    '''
    
    def __init__(self):
        super(IRConsumes, self).__init__()
        self.type = 5
        self.model = ""
        self.consumes = {}

    def activate(self, tree):
        '''Activates IR consumes. 
        
            Calculates the expected value of consumed resource, consumes the calculated 
            amount of resource, activates the IR.
            
            Args:
                tree: TaemsTree
        '''
        EVconsumed = helper_functions.calcExpectedValue(self.consumes)

        resource = tree.resources[self.To]
        if self.model == "per_time_unit":
            resource.consume(EVconsumed * tree.tasks[self.From].DurationEV, tree)
           
        elif self.model == "duration_independent":
            resource.consume(EVconsumed, tree)

        self.active = True
        #tree.activeIR.append(self)

class IRLimits(Interrelationship):
    '''A class which represents limits interrelationship.
    
        Attributes:
            model: A string, {"pet_time_click", "duration_independent"} 
                        -> in this version, only duration independent mode is implemented
            quality_power: Probability distribution of quality value which affects the destination node
            cost_power: Probability distribution of cost value which affects the destination node
            duration_power: Probability distribution of duration value which affects the destination node
            q_powerEV: A float number, expected value for quality
            d_powerEV: A float number, expected value for duration
            c_powerEV: A float number, expected value for cost
            startTime: A float number
    '''
    
    def __init__(self):
        super(IRLimits, self).__init__()
        self.type = 6
        self.model = ""
        self.quality_power = {}
        self.cost_power = {}
        self.duration_power = {}
        self.q_powerEV = -1
        self.d_powerEV = -1
        self.c_powerEV = -1
        self.startTime = None

    def activate(self, tree, time):
        '''Activates IR limits. 
        
            Modifies the IR's start time, calculates quality, cost and duration
            expected values if needed, modifies the destination's outcome, 
            activates the IR.
            
            Args:
                tree: TaemsTree
                time: A float number, current execution time
        '''

        #calculate EV only once
        if self.q_powerEV == -1:
            self.q_powerEV = helper_functions.calcExpectedValue(self.quality_power)
            self.d_powerEV = helper_functions.calcExpectedValue(self.duration_power)
            self.c_powerEV = helper_functions.calcExpectedValue(self.cost_power)

        self.apply_ir_effects(self.To, tree, time)
        
        self.active = True
        #tree.activeIR.append(self)
        
    def apply_ir_effects(self, task, tree, time):
        
        if task not in tree.tasks.keys():
            return
        
        # method
        if tree.tasks[task].subtasks is None:
            if tree.tasks[task].nonLocal:
                return
            
            if self.delay > 0:
                if self.startTime is None or self.startTime < time + self.delay:
                    self.startTime = time + self.delay

            helper_functions.mutiplyDistribution(tree.tasks[task].outcome[0], 1 - self.q_powerEV)
            if self.d_powerEV == -1:
                helper_functions.mutiplyDistribution(tree.tasks[task].outcome[1], maxint)
            else:
                helper_functions.mutiplyDistribution(tree.tasks[task].outcome[1], 1 + self.d_powerEV)
            if self.c_powerEV == -1:
                helper_functions.mutiplyDistribution(tree.tasks[task].outcome[2], maxint)
            else:
                helper_functions.mutiplyDistribution(tree.tasks[task].outcome[2], 1 + self.c_powerEV)
                
        if tree.tasks[task].subtasks is not None:
            for subtask in tree.tasks[task].subtasks:
                self.apply_ir_effects(subtask, tree, time)

    def deactivate(self, tree):
        '''Activates IR limits. 
        
            Restores the destination's outcome, deactivates the IR.
            
            Args:
                tree: TaemsTree
        '''
        helper_functions.mutiplyDistribution(tree.tasks[self.To].outcome[0], 1/(1 - self.q_powerEV))
        helper_functions.mutiplyDistribution(tree.tasks[self.To].outcome[1], 1/(1 + self.d_powerEV))
        helper_functions.mutiplyDistribution(tree.tasks[self.To].outcome[2], 1/(1 + self.c_powerEV))
        
        self.active = False
        #tree.activeIR.remove(self.label)

class IRChildOf(Interrelationship):
    '''A class which represents child of interrelationship. 
    From: parent
    To: child
    '''
    
    def __init__(self, From, To, agent):
        super(IRChildOf, self).__init__()
        self.type = 7
        self.From = From
        self.To = To
        self.agent = agent
        

class Commitment(object):

    def __init__(self):
        self.label = ""
        self.type = ""
        self.From = ""
        self.To = ""
        self.task = ""

class LocalCommitment(Commitment):

    def __init__(self):
        super(LocalCommitment, self).__init__()
        self.importance = 0
        self.min_quality = 0
        self.earliest_start_time = 0
        self.deadline = 0
        self.dont_interval_start = 0
        self.dont_interval_end = 0
        self.time_satisfied = 0

class NonLocalCommitment(Commitment):
    def __init__(self):
        super(NonLocalCommitment, self).__init__()
        self.quality_distribution = {}
        self.time_distribution = {}
