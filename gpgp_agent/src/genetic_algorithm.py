
__author__ = 'barbanas'

import simulator
from copy import deepcopy
import random
import time
from math import floor, factorial
from hashlib import md5

class GeneticAlgorithm(object):
    """
    Genetic algorithm for scheduling unordered list of tasks.
    
    Attributes:
        populationSize - integer
        population - a list of Chromosome instances
        populationID - a list of string values (ID of every chromosome in a population)
        rating - rating (based on percentage of temporal and precedence constraints met by every chromosome)
        fitness - quality of every chromosome that unites rating and total schedule length
        bestSolution - a Chromosome instance, best solution of a scheduling problem (with max fitness)
        elitePercentage - percentage of population that is automatically promoted to next generation
        worstPercentage - percentage of population that is removed from population before creating new population
        initialDurationEV - initial expected value for every methods duration (before they are scheduled)

    """

    def __init__(self, popSize, elite, worst):
        """
        Args:
            popSize - population size
            elite - percentage of population that is automatically promoted to next generation
            worst - percentage of population that is removed from population before creating new population
        """
        self.populationSize = popSize
        self.population = []
        self.populationID = []
        self.rating = []
        self.fitness = []
        self.bestSolution = None
        self.elitePercentage = elite
        self.worstPercentage = worst
        self.initialDurationEV = {}
        
    def optimize(self, alternative, nonLocalTasks, tree, noIter, scheduleStartTime):
        """
        A method that starts genetic algorithm for generating schedule from unordered list of methods.

        Args:
            alternative - a list of unordered methods (from taems task structure)
            tree - taemsTree instance that defines the task structure
            noIter - maximum number of iterations of algorithm (generations created)
        """
        sim = simulator.LightSimulator(tree)
        tasks = deepcopy(alternative)
        tasks.extend(nonLocalTasks)
        sim.execute_alternative(tasks, tasks)
        tasksToComplete = sim.completedTasks
        self.start = time.time()
        
        [chromosome, done] = self.init_population(alternative, tasksToComplete, nonLocalTasks, tree)
        if done:
            if chromosome is None:
                return None
            else:
                chromosome.evaluate(tree, sim, scheduleStartTime)
                return chromosome.schedule

        print "init  " + str(time.time() - self.start)

        self.evaluate_population(tree, tasksToComplete, sim, scheduleStartTime)

        if self.populationSize < 20:
            self.bestSolution = self.population[self.fitness.index(max(self.fitness))]
            return self.bestSolution.schedule

        iteration = 0
        iterWithoutChange = 0
        previousMaxFitness = -1

        while iteration < noIter:

            if self.bestSolution is not None:
                previousMaxFitness = self.bestSolution.fitness

            self.bestSolution = self.population[self.fitness.index(max(self.fitness))]
            
            if previousMaxFitness == self.bestSolution.fitness:
                iterWithoutChange += 1
            else:
                iterWithoutChange = 0

            if iterWithoutChange == 5:
                return self.bestSolution.schedule

            nextGen = []
            nextGenID = []
            nextGenRating = []

            self.promote_elite(nextGen, nextGenID, nextGenRating)
            self.eliminate_the_worst()

            self.create_new_generation(nextGen, nextGenID, nextGenRating, tree, tasksToComplete, sim, scheduleStartTime)
            
            self.population = [x for x in nextGen]
            self.populationID = [x for x in nextGenID]
            self.rating = [x for x in nextGenRating]  

            self.calc_fitness()
            
            print "iter  " + str(time.time() - self.start)
            iteration += 1         

    def init_population(self, alternative, tasksToComplete, nonLocalTasks, tree):
        """
        A method that creates initial population of feasible solutions to the scheduling problem.

        Args:
            alternative - a list of unordered methods (from taems task structure)
            tree - taemsTree instance that defines the task structure
        """
        self.population = []

        self.initialDurationEV = {}
        for method in alternative:
            self.initialDurationEV[method] = tree.tasks[method].DurationEV

        sim = simulator.LightSimulator(tree)

        feasibleBasePermutations = []
        numberOfPermutations = 5
        i = 0
        onlyBase = False

        scheduleDone = False
        while i < numberOfPermutations:
            [feasibleOrder, noOtherPermutations, scheduleDone] = self.create_feasible_base_order(alternative, nonLocalTasks, tasksToComplete, sim)
            feasibleBasePermutations.append(feasibleOrder)
            
            if len(feasibleBasePermutations[-1]) == 0:
                feasibleBasePermutations.pop()
                break
            
            if scheduleDone or noOtherPermutations:
                break

            if i == 0:
                if len(feasibleBasePermutations[-1]) == len(alternative):
                    numberOfPermutations = 20
                    self.populationSize = 20
                    onlyBase = True
            # method CreateFeasibleBaseOrder changes simulators atributes so it is necessary to set it up again
            sim.setup()
            i += 1
        
        if scheduleDone:
            if len(feasibleBasePermutations[-1]) == 0:
                return [None, True]
            chromosome = Chromosome(feasibleBasePermutations[-1], range(len(alternative)), self.initialDurationEV)
            chromosome.calc_id()
            return [chromosome, True]

        if len(feasibleBasePermutations) == 0:
            maxPopSize = factorial(len(alternative))
        else:
            maxPopSize = factorial(len(alternative)) / factorial(len(feasibleBasePermutations[-1]))
        
        if self.populationSize  > maxPopSize:
            self.populationSize = maxPopSize
            
        while len(self.population) < self.populationSize:
            if onlyBase:
                if len(feasibleBasePermutations) == 0:
                    self.populationSize = len(self.population)
                    return [None, False]
                methods = feasibleBasePermutations.pop()
                baseIndex = range(len(methods))
            else:
                if len(feasibleBasePermutations) == 0:
                    base = []
                else:
                    base = random.choice(feasibleBasePermutations)
                [methods, baseIndex] = self.create_solution_from_base(base, alternative)
            
            chromosome = Chromosome(methods, baseIndex, self.initialDurationEV)
            chromosome.calc_id()

            if chromosome.get_id() in self.populationID:
                continue

            self.population.append(chromosome)
            self.populationID.append(chromosome.get_id())

        return [None, False]
            
    def create_feasible_base_order(self, alternative, nonLocalTasks, tasksToComplete, sim):
        """
        A method that creates a feasible order of hard-constrained methods in alternative (base of schedule).

        Args:
            alternative - unordered list of methods to schedule
            sim - LightSimulator instance, it is used to simulate effects of activation of hard constraints
        """
        toComplete = list(set(alternative) | set(nonLocalTasks))
        sim.init_disablements(toComplete, tasksToComplete)
        #sim.execute_alternative(nonLocalTasks, tasksToComplete)

        hardConstrained = list(sim.hardConstrainedTasks)

        if len(hardConstrained) == 0:
            return [[], 0, 0]

        temp = deepcopy(hardConstrained)
        feasibleBaseOrder = []
        
        numberOfOptions = 0
        while True:
            possibleNext = sim.get_enabled_tasks(temp)
            if len(set(possibleNext) & set(toComplete)) == 0:
                break
            nonLocalNext = set(nonLocalTasks) & set(possibleNext)
            possibleNext = set(alternative) & set(possibleNext)

            sim.execute_alternative(nonLocalNext, tasksToComplete)
            temp = list(set(temp) - set(nonLocalNext))
            numberOfOptions += len(possibleNext)

            if len(possibleNext) > 0:
                feasibleBaseOrder.append(random.sample(possibleNext, 1)[0])
                temp.remove(feasibleBaseOrder[-1])
                if len(temp) == 0:
                    break
                sim.execute_task(feasibleBaseOrder[-1], tasksToComplete)

        if numberOfOptions == len(alternative):
            return [feasibleBaseOrder, 1, 1]
        if numberOfOptions == len(hardConstrained):
            return [feasibleBaseOrder, 1, 0]
        elif numberOfOptions == 0:
            return [[], 1, 0]
        else:
            return [feasibleBaseOrder, 0, 0]

    def create_solution_from_base(self, base, alternative):
        """
        A method that creates solution from ordered list of base methods. First it selects indices for base
        methods and puts them into selected positions. Then all other methods are permuted in random order and
        put in available places in the schedule.

        Args:
            base - list of ordered base methods
            alternaive - unordered list of methods to schedule
        """
        schedule = ["" for i in range(len(alternative))]
        baseIndex = []
        x = -1

        # 1. position base
        temp = deepcopy(base)
        for i in range(len(base)):
            if x == -1:
                x = random.randint(0, len(alternative) - len(temp))
            else:
                x = random.randint(x + 1, len(alternative) - len(temp))
            
            schedule[x] = base[i]
            baseIndex.append(x)
            temp.remove(base[i])

            if len(alternative) - x - 1 == len(temp):
                schedule[x + 1 :] = temp[:]
                baseIndex.extend(range(x + 1, len(schedule)))
                break

        if len(schedule) == len(base):
            return [schedule, baseIndex]

        # 2. put other tasks into schedule
        permutation = GeneticAlgorithm.permutation(list(set(alternative) - set(base)))

        for i in range(len(schedule)):
            if schedule[i] == "":
                schedule[i] = permutation.pop(0)
                if len(permutation) == 0:
                    break
        
        return [schedule, baseIndex]

    @staticmethod
    def permutation(array):
        """
        Creates random permutation from a array.

        Args:
            array - a list of elements to premute
        """
        perm = []
        temp = deepcopy(array)

        while len(temp) > 0:
            perm.append(temp.pop(random.randint(0, len(temp)-1)))
            
        return perm

    def evaluate_population(self, tree, tasksToComplete, sim, startTime):
        """
        A method that evaluates population by calculating its rating and fitness.

        Args:
            tree - taemsTree instance that holds the task structure
        """
        self.populationID = []
        self.rating = []
        
        for chromosome in self.population:
            self.rating.append(chromosome.evaluate(tree, sim, startTime))
            self.populationID.append(chromosome.get_id())
        
        self.calc_fitness()

        print "eval  " + str(time.time() - self.start)

    def calc_fitness(self):    
        """
        A method that calculates fitness of every chromosome in population. It combines previously calculated
        chromosome rating with total duration of every chromosomes schedule.

        Args:
            None
        """
        self.fitness = []
        maxScheduleDuration = -1

        for chromosome in self.population:
            if chromosome.scheduleDuration > maxScheduleDuration:
                maxScheduleDuration = chromosome.scheduleDuration

        for chromosome in self.population:
            string = str(int(maxScheduleDuration - chromosome.scheduleDuration))
            string = string.zfill(8)
            self.fitness.append(float(str(int(chromosome.rating * 10000))+ "." + string))

    def promote_elite(self, nextGen, nextGenID, nextGenRating):
        """
        A method that puts elitePercentage of best chromosomes in population into next generation.

        Args:
            nextGen (out) - list of chromosomes in next generation
            nextGenID (out) - list of next generation chromosome IDs
            nextGenRating (out) - list of next generation chromosome ratings
            
            ***(out) arguments are empty lists and are partially populated in this method
        """
        eliteNumber = int(floor(self.populationSize * self.elitePercentage))
        temp = deepcopy(self.fitness)

        while eliteNumber > 0:
            x = temp.index(max(temp))
            temp[x] = -1

            nextGen.append(self.population[x])
            nextGenID.append(self.populationID[x])
            nextGenRating.append(self.rating[x])

            eliteNumber -= 1

    def eliminate_the_worst(self):
        """
        A method that removes worstPercentage of chromosomes with lowest fitness from current population. 
        They do not participate in transferring their genes into next generation.
        """
        worstNumber = int(floor(self.populationSize * self.worstPercentage))
        
        removed = 0
        while removed < worstNumber:
            x = self.fitness.index(min(self.fitness))

            self.fitness.pop(x)
            self.population.pop(x)
            self.populationID.pop(x)

            removed += 1

    def create_new_generation(self, nextGen, nextGenID, nextGenRating, tree, tasksToComplete, sim, startTime):
        """
        A method that populates next generation with chromosomes created by applying genetic operators
        to parents from current population.
        
        Args:
            nextGen (out) - list of chromosomes in next generation
            nextGenID (out) - list of next generation chromosome IDs
            nextGenRating (out) - list of next generation chromosome ratings
            tree - taemsTree instance that holds the task structure
        """
        while len(nextGen) < self.populationSize:
            parents = []
            # select a parent
            parents.append(random.randint(0, len(self.population) - 1))

            if random.random() < 0.5:
                # mutation
                if random.random() < 0.5:
                    children = self.mutation_order_based(parents[0], tree)
                else:
                    children = self.mutation_position_based(parents[0], tree)
            else:
                # crossover
                # select another parent
                parents.append(random.randint(0, len(self.population) - 1))
                while parents[1] == parents[0]:
                    parents[1] = random.randint(0, len(self.population) - 1)

                if random.random() < 0.5:
                    children = self.crossover_position_based(parents, tree)
                else:
                    children = self.crossover_order_based(parents, tree)

            for child in children:
                child.evaluate(tree, sim, startTime)
                nextGen.append(child)
                nextGenID.append(child.ID)
                nextGenRating.append(child.rating)

    def crossover_position_based(self, parents, tree):
        """
        Position based crossover. Children get order of (all) methods from one parent and base method
        positions from other parent. It only changes the position of base methods.

        Args:
            parents - two Chromosome instances
            tree - taemsTree instance that holds the task structure
        """
        p1 = self.population[parents[0]]
        p2 = self.population[parents[1]]

        child1 = Chromosome(["" for i in range(len(p1.genes))], p2.base, self.initialDurationEV)
        child2 = Chromosome(["" for i in range(len(p2.genes))], p1.base, self.initialDurationEV)

        # set up base
        for i in range(len(child1.base)):
            child2.genes[child2.base[i]] =  p2.genes[p2.base[i]]
            child1.genes[child1.base[i]] =  p1.genes[p1.base[i]]

        i1 = 0
        i2 = 0
        for i in range(len(p1.genes)):
            if len(child2.genes[i]) == 0:
                while i2 in p2.base:
                    i2 += 1
                child2.genes[i] =  p2.genes[i2]
                i2 += 1

            if len(child1.genes[i]) == 0:
                while i1 in p1.base:
                    i1 += 1
                child1.genes[i] =  p1.genes[i1]
                i1 += 1
         
        child1.calc_id()
        child2.calc_id()

        result = []
        if child1.ID not in self.populationID:
            result.append(child1)

        if child2.ID not in self.populationID:
            result.append(child2)

        return result

    def crossover_order_based(self, parents, tree):
        """
        Position based crossover. Children get base method positions from one parent and base method order
        form other. It doesn't change the position of base methods, the only change is order of base.

        Args:
            parents - two Chromosome instances
            tree - taemsTree instance that holds the task structure
        """
        p1 = self.population[parents[0]]
        p2 = self.population[parents[1]]

        child1 = Chromosome(["" for i in range(len(p1.genes))], p1.base, self.initialDurationEV)
        child2 = Chromosome(["" for i in range(len(p2.genes))], p2.base, self.initialDurationEV)

        # set up base
        for i in range(len(child1.base)):
            child2.genes[child2.base[i]] =  p1.genes[p1.base[i]]
            child1.genes[child1.base[i]] =  p2.genes[p2.base[i]]

        i1 = 0
        i2 = 0
        for i in range(len(p1.genes)):
            if len(child2.genes[i]) == 0:
                while i1 in p1.base:
                    i1 += 1
                child2.genes[i] =  p1.genes[i1]
                i1 += 1

            if len(child1.genes[i]) == 0:
                while i2 in p2.base:
                    i2 += 1
                child1.genes[i] =  p2.genes[i2]
                i2 += 1
         
        child1.calc_id()
        child2.calc_id()

        result = []
        if child1.ID not in self.populationID:
            result.append(child1)

        if child2.ID not in self.populationID:
            result.append(child2)

        return result

    def mutation_order_based(self, parent, tree):
        """
        Order based mutation. It leaves base positions as they are in a parent and randomly permutes
        non-base methods.

        Args:
            parent - Chromosome instance
            tree - taemsTree instance that holds the task structure
        """
        p = self.population[parent]
        nonBase = list(set(p.genes) - set(p.get_base()))

        child = Chromosome(["" for i in range(len(p.genes))], p.base, self.initialDurationEV)

        for i in range(len(child.base)):
            child.genes[child.base[i]] =  p.genes[p.base[i]]

        nonBase = GeneticAlgorithm.permutation(nonBase)
        i1 = 0
        for i in range(len(p.genes)):
            if len(child.genes[i]) == 0:
                child.genes[i] =  nonBase[i1]
                i1 += 1
        
        child.calc_id()

        if child.ID not in self.populationID:
            return [child]

        return []

    def mutation_position_based(self, parent, tree):
        """
        Position based mutation. Order of methods stays as it is in a parent, new positions of
        base elements are randomly chosen.

        Args:
            parent - Chromosome instance
            tree - taemsTree instance that holds the task structure
        """
        p = self.population[parent]

        base = random.sample(range(len(p.genes)), len(p.base))
        base.sort()
        child = Chromosome(["" for i in range(len(p.genes))], base, self.initialDurationEV)

        for i in range(len(child.base)):
            child.genes[child.base[i]] =  p.genes[p.base[i]]

        i1 = 0
        for i in range(len(p.genes)):
            if len(child.genes[i]) == 0:
                while i1 in p.base:
                    i1 += 1
                child.genes[i] =  p.genes[i1]
                i1 += 1
        
        child.calc_id()

        if child.ID not in self.populationID:
            return [child]

        return []

class Chromosome(object):
    """

    Attributes:
        genes - a list of methods in a schedule
        base - a list of positions of base methods
        rating - chromosome rating (for explanation see GeneticAlgorithm description)
        fitness - chromosome fitness (for explanation see GeneticAlgorithm description)
        durationEV - dictionary : { key = method label, value = expected value of duration}
        schedule - list [[method1 label, method1 start time, method1 end time], ...  ]
        ID - id of a chromosome - unique identifier that is calculated from order of methods in a genome
        scheduleDuration - expected duration of schedule
    """
    def __init__(self, genes, base, initialDurationEV):
        """
        Args:
             genes - an ordered list of methods
             base - a list of positions of base methods
             initialDurationEV - initial expected value for every methods duration (before they are scheduled)
        """
        self.genes = deepcopy(genes)
        self.base = deepcopy(base)
        self.rating = 0
        self.fitness = 0
        self.durationEV = deepcopy(initialDurationEV)
        self.schedule = []
        self.ID = id
        self.scheduleDuration = 0

    def evaluate(self, tree, sim, time):
        """
        A method that creates a schedule from ordered list of methods (self.genes). It also calculates
        rating of created schedule.

        Args:
            tree - taemsTree instance that holds the task structure
        """
        self.calc_id()
        self.completedTasks = []

        startTime = [time]
        noSoftIRActivated = [0, 0]  # [facilitates, hinders]
        noSoftIR = [0, 0]
        self.schedule = []
        deadlines = [0, 0] # [total number of deadlines, number of broken deadlines]
        earliestStartTimes = [0, 0] # [total number of earliest start times, number of broken est]

        sim.setup()
        sim.execute_alternative(self.genes, self.genes)
        
        for ir in tree.IRs.values():
            if ir.From in sim.completedTasks and ir.To in sim.completedTasks:
                if ir.type < 2 or ir.type > 3:
                    continue
                noSoftIR[ir.type - 2] += 1
                # dodaj da se aktivira efekt soft ir -> dodano -> testiraj
                if sim.completedTasks.index(ir.From) < sim.completedTasks.index(ir.To):
                    noSoftIRActivated[ir.type - 2] += 1
                    if ir.type == 2:
                        self.durationEV[ir.To] /= ir.d_powerEV
                    if ir.type == 3:
                        self.durationEV[ir.To] *= ir.d_powerEV

        for method in self.genes:
            if tree.tasks[method].earliestStartTime is not None:
                diff = tree.tasks[method].earliestStartTime - startTime[-1]
                # insert slack
                if diff > 0:
                    startTime.append(startTime[-1] + diff)
                    self.schedule.append(["slack", startTime[-2], startTime[-1]])

            if tree.tasks[method].deadline is not None:
                deadlines[0] += 1
                if startTime[-1] + tree.tasks[method].DurationEV - tree.tasks[method].deadline > 0:
                    deadlines[1] += 1

            if tree.tasks[method].earliestStartTime is not None:
                earliestStartTimes[0] += 1
                if tree.tasks[method].earliestStartTime - startTime[-1] > 0:
                    earliestStartTimes[1] += 1

            startTime.append(startTime[-1] + self.durationEV[method])
            self.schedule.append([method, startTime[-2], startTime[-1]])

        self.scheduleDuration = self.schedule[-1][2]

        # create fitness
        # 2. punish broken deadlines and earliest start times
        rating2 = 1
        if deadlines[0] > 0:
            rating2 = 1.01 - deadlines[1] /  float(deadlines[0])

        rating3 = 1
        if earliestStartTimes[0] > 0:
            rating3 = 1.01 - earliestStartTimes[1] / float(earliestStartTimes[0])

        # 3. reward activated positive soft ir and punish negative soft ir
        rating4 = 0
        if noSoftIR[0] > 0:
            rating4 = noSoftIRActivated[0] / float(noSoftIR[0])
        
        rating5 = 0
        if noSoftIR[0] > 0:
            rating5 = - noSoftIRActivated[1] / float(noSoftIR[1])


        totalRating = 100 * rating2 * rating3 * (1 + rating4 + rating5)
        self.rating = totalRating
        return totalRating

    def calc_id(self):
        string = ""
        for x in self.genes:
            string += x

        self.ID = md5(string).digest()

    def get_id(self):
        return self.ID

    def get_base(self):
        """
        A method that returns chromosome base methods - elements on self.base positions.
        """
        baseMethods = []
        for x in self.base:
            baseMethods.append(self.genes[x])

        return baseMethods