#Repository for GPGP agent.

Master branch contains core GPGP code. 

Every other branch is an implementation of specific distributed task allocation problem/scenario.    
To run the code, you have to checkout the master branch as well as the particular scenario branch.